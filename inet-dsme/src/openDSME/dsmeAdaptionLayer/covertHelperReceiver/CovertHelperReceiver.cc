#include "./CovertHelperReceiver.h"

#include "../../dsmeLayer/DSMELayer.h"
#include "../../mac_services/mcps_sap/MCPS_SAP.h"
#include "../../mac_services/mlme_sap/MLME_SAP.h"
#include "../../mac_services/pib/MAC_PIB.h"
#include <omnetpp.h>
#include <math.h>
#include <chrono>
#include <thread>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

namespace dsme {

    CovertHelperReceiver::CovertHelperReceiver(DSMEAdaptionLayer& dsmeAdaptionLayer)
        : dsmeAdaptionLayer(dsmeAdaptionLayer){

    }

    void CovertHelperReceiver::initialize(void) {
        char possibleBits[] = {'0', '1'};
        printAllKLength(possibleBits, 8, 2);
        diffCodes[0][0] = "0000011011001000";
        diffCodes[1][0] = "0000011011001000";
    }

    void CovertHelperReceiver::initializeConfigs(std::string tech_new, uint8_t techBytes_new, uint8_t techAutBytes_new){
        tech = tech_new;
        techBytes = techBytes_new;
        techAutBytes = techAutBytes_new;
        tech.erase(std::remove(tech.begin(), tech.end(), '\"'), tech.end());
    }

    int CovertHelperReceiver::getKeyLength(){
        return 16/unsigned(techAutBytes);
    }

    void CovertHelperReceiver::readCovertDelay(mcps_sap::DATA_indication_parameters& params){
        if(Autentication == 0){
            if (unsigned(techAutBytes) == 1){
                autentication(params);
            }
            else if (unsigned(techAutBytes) == 2){
                autentication2(params);
            }
            else if ((unsigned(techAutBytes) == 4)){
                autentication4(params);
            }
            else if ((unsigned(techAutBytes) == 8)){
                autentication8(params);
            }

        } 
        else if (Autentication == 1)
        {
        if (tech.compare("OnOff") == 0){
            readCovertOnOff(params);
        }else if (tech.compare("LBitsNPackets") == 0){
            if (unsigned(techBytes) == 2)
                readCovertList(params);
            else if (unsigned(techBytes) == 4)
                readCovertListExp(params);
            else if (unsigned(techBytes) == 8)
                readCovertListExp8(params);
        }else if (tech.compare("TimeReplay") == 0){
            if (unsigned(techBytes) == 2)
                readCovert2Lists(params);
            else if (unsigned(techBytes) == 4)
                readCovert2ListsExp(params);
            else if (unsigned(techBytes) == 8)
                readCovert2ListsExp8(params);
        }else
            DSME_SIM_ASSERT(false);
        }
        }

    char CovertHelperReceiver::getCharFromBitset(std::bitset<8> s){
        unsigned long i = s.to_ulong(); 
        return static_cast<char>( i );
    }

    void CovertHelperReceiver::readCovertOnOff(mcps_sap::DATA_indication_parameters& params){
        double millis = (double) omnetpp::simTime().dbl();

        uint8_t so = unsigned(this->dsmeAdaptionLayer.getMAC_PIB().macSuperframeOrder);
        uint16_t byteSize = (params.msdu->getTotalSymbols() - 120U) / 2;
        uint32_t symbols = const_redefines::macLIFSPeriod * 3;

        std::ofstream myfile;

        double time = ((symbols * 15.36) / 960) + (byteSize * 0.032) + 3.104;
        double time1 = (((symbols - const_redefines::macLIFSPeriod) * 15.36) / 960) + (byteSize * 0.032) + 3.104;

        if(params.msdu->getHeader().getDestAddr().getShortAddress() == 1 && params.msdu->getHeader().getSrcAddr().getShortAddress() == 2){
            currentInterval = (millis - lastPacketTime) * 1000.0;
            int16_t symbolsTmp = ((((currentInterval - 3.104 - (byteSize * 0.032)) * 960) / 15.36) + 0.5);
            if(symbolsTmp == 1){
                rMessageInBits.none();
                rLetterCounter = 0;
                messageToDecode.clear();
            } else if(currentInterval < 100){
                if(symbolsTmp == 0){
                    messageTemp = diffChars[0];
                }
                if(symbolsTmp == 2+ALPHA){
                    messageTemp = diffChars[1];
                }
                myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                myfile << (std::to_string(currentInterval) + ",0" + "\n");
                myfile.close();
                char charMessageTmp = messageTemp;
                messageToDecode.append(1, charMessageTmp);
                rLetterCounter++;
                LOG_DEBUG("CHAR: " << messageTemp);
                LOG_DEBUG("MESSAGE:" << messageToDecode);
            } else if(currentInterval < 40000) {
                myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                myfile << ("0," + std::to_string(currentInterval) + "\n");
                myfile.close(); 
            }
            
            lastPacketTime = millis;
        }
    }

    void CovertHelperReceiver::readCovertList(mcps_sap::DATA_indication_parameters& params){

        // 0ms <= 0 <= 6ms <= 1 <= 9ms <= nothing

        /*using std::chrono::system_clock;
        using std::chrono::duration_cast;
        using std::chrono::milliseconds;
        using std::chrono::system_clock;*/
                    
        //auto millis = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();


        std::string diffChars2[] {"00", "01", "10", "11"};

        /*diffChars.clear();
        diffChars.push_back('00');
        diffChars.push_back('01');
        diffChars.push_back('10');
        diffChars.push_back('11');*/

        double millis = (double) omnetpp::simTime().dbl();
        LOG_DEBUG("ADDDDDRESSSSSSSSSSSS: " << params.msdu->getHeader().getSrcAddr().getShortAddress());
        LOG_DEBUG("ADDDDDRESSSSSSSSSSSSD: " << params.msdu->getHeader().getDestAddr().getShortAddress());
        LOG_DEBUG("LPT: " << lastPacketTime);
        LOG_DEBUG("SIZEEEEE: " << params.msdu->getTotalSymbols());
        uint8_t so = unsigned(this->dsmeAdaptionLayer.getMAC_PIB().macSuperframeOrder);
        uint16_t byteSize = (params.msdu->getTotalSymbols() - 120U) / 2;
        LOG_DEBUG("BYTESIZEEEEE: " << byteSize);
        std::ofstream myfile;

        // SYMBOLS:
        // base superframe duration = 960
        // base slot duration = 60
        // 1.5% of slot duration --> singular delay

        uint32_t symbols = const_redefines::macLIFSPeriod * 3;//60U * (pow(2, unsigned(so))) * 0.015;

        // TIME:
        // 960 symbols --> 15.36 ms
        // symbols symbols --> y ms
        // Then we add 3.104 ms that is the constant delay time when no delay is added to the packet and the packet length is 0B

        double time = ((symbols * 15.36) / 960) + (byteSize * 0.032) + 3.104;
        double time1 = (((symbols - const_redefines::macLIFSPeriod) * 15.36) / 960) + (byteSize * 0.032) + 3.104;
        LOG_DEBUG("PERFECT INTERVAL: " << time);
        LOG_DEBUG("PERFECT INTERVAL: " << time1);

        if(params.msdu->getHeader().getDestAddr().getShortAddress() == 1 && params.msdu->getHeader().getSrcAddr().getShortAddress() == 2){
            currentInterval = (millis - lastPacketTime) * 1000.0;


            int16_t symbolsTmp = ((((currentInterval - 3.104 - (byteSize * 0.032)) * 960) / 15.36) + 0.5);// / const_redefines::macLIFSPeriod;

            LOG_DEBUG("AAAAAAAAAAAAAAAAAAAAAAAAAAA " << symbols << " " << symbolsTmp);
            LOG_DEBUG("SYMBOLS ACT: " << symbols);
            LOG_DEBUG("SYMBOLS PRED: " << symbolsTmp);

            LOG_DEBUG(omnetpp::simTime());
            LOG_DEBUG("LAST: " << lastPacketTime);
            LOG_DEBUG("NOW: " << millis);
            LOG_DEBUG("INTERVAL: " << currentInterval);

            if(symbolsTmp == 1){
                rMessageInBits.none();
                rLetterCounter = 0;
                messageToDecode.clear();
            } else if(currentInterval < 100){
                symbolsTmp = symbolsTmp / ALPHA;
                myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                myfile << (std::to_string(currentInterval) + ",0" + "\n");
                myfile.close();
                //messageTemp = diffChars2[symbolsTmp-2];
                LOG_DEBUG(diffChars2[(symbolsTmp-2)/ALPHA]);
                LOG_DEBUG("COUNTER: " << rBitCounter);
                LOG_DEBUG("MESSAGE TEMP: " << messageTemp);
                std::string charMessageTmp = diffChars2[symbolsTmp-2];
                //if(rBitCounter >= 8){
                    //messageTemp.erase(std::remove_if(messageTemp.begin(), messageTemp.end(), ::isspace), messageTemp.end());
                    messageToDecode += charMessageTmp;
                    rLetterCounter++;
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG("CHAR: " << messageTemp);
                    LOG_DEBUG("MESSAGE:" << messageToDecode);
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                //}
            } else if(currentInterval < 40000) {
                myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                myfile << ("0," + std::to_string(currentInterval) + "\n");
                myfile.close(); 
            }
            
            lastPacketTime = millis;
        }
    }
    
    void CovertHelperReceiver::readCovertListExp(mcps_sap::DATA_indication_parameters& params){

        // 0ms <= 0 <= 6ms <= 1 <= 9ms <= nothing

        /*using std::chrono::system_clock;
        using std::chrono::duration_cast;
        using std::chrono::milliseconds;
        using std::chrono::system_clock;*/
                    
        //auto millis = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();

        std::vector<std::string> possibleCombinations {
            "0000",
            "0001",
            "0010",
            "0011",
            "0100",
            "0101",
            "0110",
            "0111",
            "1000",
            "1001",
            "1010",
            "1011",
            "1100",
            "1101",
            "1110",
            "1111",
        };

        /*diffChars.clear();
        diffChars.push_back('00');
        diffChars.push_back('01');
        diffChars.push_back('10');
        diffChars.push_back('11');*/

        double millis = (double) omnetpp::simTime().dbl();
        LOG_DEBUG("ADDDDDRESSSSSSSSSSSS: " << params.msdu->getHeader().getSrcAddr().getShortAddress());
        LOG_DEBUG("ADDDDDRESSSSSSSSSSSSD: " << params.msdu->getHeader().getDestAddr().getShortAddress());
        LOG_DEBUG("LPT: " << lastPacketTime);
        LOG_DEBUG("SIZEEEEE: " << params.msdu->getTotalSymbols());
        uint8_t so = unsigned(this->dsmeAdaptionLayer.getMAC_PIB().macSuperframeOrder);
        uint16_t byteSize = (params.msdu->getTotalSymbols() - 120U) / 2;
        LOG_DEBUG("BYTESIZEEEEE: " << byteSize);
        std::ofstream myfile;

        // SYMBOLS:
        // base superframe duration = 960
        // base slot duration = 60
        // 1.5% of slot duration --> singular delay

        uint32_t symbols = const_redefines::macLIFSPeriod * 3;//60U * (pow(2, unsigned(so))) * 0.015;

        // TIME:
        // 960 symbols --> 15.36 ms
        // symbols symbols --> y ms
        // Then we add 3.104 ms that is the constant delay time when no delay is added to the packet and the packet length is 0B

        double time = ((symbols * 15.36) / 960) + (byteSize * 0.032) + 3.104;
        double time1 = (((symbols - const_redefines::macLIFSPeriod) * 15.36) / 960) + (byteSize * 0.032) + 3.104;
        LOG_DEBUG("PERFECT INTERVAL: " << time);
        LOG_DEBUG("PERFECT INTERVAL: " << time1);

        if(params.msdu->getHeader().getDestAddr().getShortAddress() == 1 && params.msdu->getHeader().getSrcAddr().getShortAddress() == 2){
            currentInterval = (millis - lastPacketTime) * 1000.0;


            uint32_t symbolsTmp = ((((currentInterval - 3.104 - (byteSize * 0.032)) * 960) / 15.36) + 0.5);// / const_redefines::macLIFSPeriod;

            LOG_DEBUG("AAAAAAAAAAAAAAAAAAAAAAAAAAA " << symbols << " " << symbolsTmp);
            LOG_DEBUG("SYMBOLS ACT: " << symbols);
            LOG_DEBUG("SYMBOLS PRED: " << symbolsTmp);

            LOG_DEBUG(omnetpp::simTime());
            LOG_DEBUG("LAST: " << lastPacketTime);
            LOG_DEBUG("NOW: " << millis);
            LOG_DEBUG("INTERVAL: " << currentInterval);

            if(symbolsTmp == 1){
                rMessageInBits.none();
                rLetterCounter = 0;
                messageToDecode.clear();
            } else if(currentInterval < 100){
                myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                myfile << (std::to_string(currentInterval) + ",0" + "\n");
                myfile.close();
                //messageTemp = diffChars2[symbolsTmp-2];
                //LOG_DEBUG(diffChars2[symbolsTmp-2]);
                LOG_DEBUG("COUNTER: " << rBitCounter);
                LOG_DEBUG("MESSAGE TEMP: " << messageTemp);
                std::string charMessageTmp = possibleCombinations.at((symbolsTmp/ALPHA));
                //if(rBitCounter >= 8){
                    //messageTemp.erase(std::remove_if(messageTemp.begin(), messageTemp.end(), ::isspace), messageTemp.end());
                    messageToDecode += charMessageTmp;
                    rLetterCounter++;
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG("CHAR: " << messageTemp);
                    LOG_DEBUG("MESSAGE:" << messageToDecode);
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                //}
            } else if(currentInterval < 40000) {
                myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                myfile << ("0," + std::to_string(currentInterval) + "\n");
                myfile.close(); 
            }
            
            lastPacketTime = millis;
        }
    }
    
    void CovertHelperReceiver::readCovertListExp8(mcps_sap::DATA_indication_parameters& params){

        // 0ms <= 0 <= 6ms <= 1 <= 9ms <= nothing

        /*using std::chrono::system_clock;
        using std::chrono::duration_cast;
        using std::chrono::milliseconds;
        using std::chrono::system_clock;*/
                    
        //auto millis = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();

        /*diffChars.clear();
        diffChars.push_back('00');
        diffChars.push_back('01');
        diffChars.push_back('10');
        diffChars.push_back('11');*/

        double millis = (double) omnetpp::simTime().dbl();
        LOG_DEBUG("ADDDDDRESSSSSSSSSSSS: " << params.msdu->getHeader().getSrcAddr().getShortAddress());
        LOG_DEBUG("ADDDDDRESSSSSSSSSSSSD: " << params.msdu->getHeader().getDestAddr().getShortAddress());
        LOG_DEBUG("LPT: " << lastPacketTime);
        LOG_DEBUG("SIZEEEEE: " << params.msdu->getTotalSymbols());
        uint8_t so = unsigned(this->dsmeAdaptionLayer.getMAC_PIB().macSuperframeOrder);
        uint16_t byteSize = (params.msdu->getTotalSymbols() - 120U) / 2;
        LOG_DEBUG("BYTESIZEEEEE: " << byteSize);
        std::ofstream myfile;

        // SYMBOLS:
        // base superframe duration = 960
        // base slot duration = 60
        // 1.5% of slot duration --> singular delay

        uint32_t symbols = const_redefines::macLIFSPeriod * 3;//60U * (pow(2, unsigned(so))) * 0.015;

        // TIME:
        // 960 symbols --> 15.36 ms
        // symbols symbols --> y ms
        // Then we add 3.104 ms that is the constant delay time when no delay is added to the packet and the packet length is 0B

        double time = ((symbols * 15.36) / 960) + (byteSize * 0.032) + 3.104;
        double time1 = (((symbols - const_redefines::macLIFSPeriod) * 15.36) / 960) + (byteSize * 0.032) + 3.104;
        LOG_DEBUG("PERFECT INTERVAL: " << time);
        LOG_DEBUG("PERFECT INTERVAL: " << time1);

        if(params.msdu->getHeader().getDestAddr().getShortAddress() == 1 && params.msdu->getHeader().getSrcAddr().getShortAddress() == 2){
            currentInterval = (millis - lastPacketTime) * 1000.0;

            int16_t symbolsTmp = ((((currentInterval - 3.104 - (byteSize * 0.032)) * 960) / 15.36) + 0.5);// / const_redefines::macLIFSPeriod;

            LOG_DEBUG("AAAAAAAAAAAAAAAAAAAAAAAAAAA " << symbols << " " << symbolsTmp);
            //LOG_DEBUG("SYMBOLS ACT: " << symbols);
            LOG_DEBUG("SYMBOLS PRED: " << symbolsTmp);

            LOG_DEBUG(omnetpp::simTime());
            LOG_DEBUG("LAST: " << lastPacketTime);
            LOG_DEBUG("NOW: " << millis);
            LOG_DEBUG("INTERVAL: " << currentInterval);

            if(symbolsTmp == 1){
                rMessageInBits.none();
                rLetterCounter = 0;
                messageToDecode.clear();
            } else if(currentInterval < 100){
                myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                myfile << (std::to_string(currentInterval) + ",0" + "\n");
                myfile.close();
                //messageTemp = diffChars2[symbolsTmp-2];
                //LOG_DEBUG(diffChars2[symbolsTmp-2]);
                LOG_DEBUG("COUNTER: " << rBitCounter);
                LOG_DEBUG("MESSAGE TEMP: " << messageTemp);
                LOG_DEBUG(possibleCombinations8.size());
                std::string charMessageTmp = possibleCombinations8.at((symbolsTmp-2)/ALPHA);
                //if(rBitCounter >= 8){
                    //messageTemp.erase(std::remove_if(messageTemp.begin(), messageTemp.end(), ::isspace), messageTemp.end());
                    messageToDecode += charMessageTmp;
                    rLetterCounter++;
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG("CHAR: " << messageTemp);
                    LOG_DEBUG("MESSAGE:" << messageToDecode);
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                //}
            } else if(currentInterval < 40000) {
                myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                myfile << ("0," + std::to_string(currentInterval) + "\n");
                myfile.close(); 
            }
            
            lastPacketTime = millis;
        }
    }

    void CovertHelperReceiver::readCovert2Lists(mcps_sap::DATA_indication_parameters& params){

        // 0ms <= 0 <= 6ms <= 1 <= 9ms <= nothing

        /*using std::chrono::system_clock;
        using std::chrono::duration_cast;
        using std::chrono::milliseconds;
        using std::chrono::system_clock;*/
                    
        //auto millis = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();

        std::vector<uint8_t> diffIntervals00 {2,6,10};
        std::vector<uint8_t> diffIntervals01 {3,7,11};
        std::vector<uint8_t> diffIntervals10 {4,8,12};
        std::vector<uint8_t> diffIntervals11 {5,9,13};

        double millis = (double) omnetpp::simTime().dbl();
        LOG_DEBUG("ADDDDDRESSSSSSSSSSSS: " << params.msdu->getHeader().getSrcAddr().getShortAddress());
        LOG_DEBUG("ADDDDDRESSSSSSSSSSSSD: " << params.msdu->getHeader().getDestAddr().getShortAddress());
        LOG_DEBUG("LPT: " << lastPacketTime);
        LOG_DEBUG("SIZEEEEE: " << params.msdu->getTotalSymbols());
        uint8_t so = unsigned(this->dsmeAdaptionLayer.getMAC_PIB().macSuperframeOrder);
        uint16_t byteSize = (params.msdu->getTotalSymbols() - 120U) / 2;
        LOG_DEBUG("BYTESIZEEEEE: " << byteSize);
        std::ofstream myfile;

        // SYMBOLS:
        // base superframe duration = 960
        // base slot duration = 60
        // 1.5% of slot duration --> singular delay

        uint32_t symbols = const_redefines::macLIFSPeriod * 3;//60U * (pow(2, unsigned(so))) * 0.015;

        // TIME:
        // 960 symbols --> 15.36 ms
        // symbols symbols --> y ms
        // Then we add 3.104 ms that is the constant delay time when no delay is added to the packet and the packet length is 0B

        double time = ((symbols * 15.36) / 960) + (byteSize * 0.032) + 3.104;
        //double time1 = (((symbols - const_redefines::macLIFSPeriod) * 15.36) / 960) + (byteSize * 0.032) + 3.104;
        LOG_DEBUG("PERFECT INTERVAL: " << time);
        //LOG_DEBUG("PERFECT INTERVAL: " << time1);

        if(params.msdu->getHeader().getDestAddr().getShortAddress() == 1 && params.msdu->getHeader().getSrcAddr().getShortAddress() == 2){
            currentInterval = (millis - lastPacketTime) * 1000.0;


            int16_t symbolsTmp = ((((currentInterval - 3.104 - (byteSize * 0.032)) * 960) / 15.36) + 0.5);// / const_redefines::macLIFSPeriod;
            std::cout << "SYMBOLSTEMP - " << symbolsTmp << std::endl;

            LOG_DEBUG("AAAAAAAAAAAAAAAAAAAAAAAAAAA " << symbols << " " << symbolsTmp);
            LOG_DEBUG("SYMBOLS ACT: " << symbols);
            LOG_DEBUG("SYMBOLS PRED: " << symbolsTmp);

            LOG_DEBUG(omnetpp::simTime());
            LOG_DEBUG("LAST: " << lastPacketTime);
            LOG_DEBUG("NOW: " << millis);
            LOG_DEBUG("INTERVAL: " << currentInterval);

            std::cout << "INTERVAL - " << currentInterval << std::endl;
            std::cout << "BYTE SIZE - " << byteSize << std::endl;
            std::cout << "INTERVAL MINUS - " << ((((currentInterval - 3.104 - (byteSize * 0.032)) * 960) / 15.36) + 0.5) << std::endl;
            /*
            if(currentInterval < (time1 - 0.2)){
                messageTemp.append(1, '0');
                LOG_DEBUG("0");
                myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                myfile << (std::to_string(currentInterval) + ",0" + "\n");
                myfile.close(); 
                rBitCounter++;
            }else if(currentInterval < (time - 0.2) || (currentInterval > (time + 0.2) && currentInterval < 100)){
                messageTemp.append(1, '1');
                LOG_DEBUG("1");
                myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                myfile << (std::to_string(currentInterval) + ",0" + "\n");
                myfile.close(); 
                rBitCounter++;
            }else if(currentInterval < (time + 0.2)){
                rMessageInBits.none();
                rBitCounter = 0;
                rLetterCounter = 0;
                messageTemp.clear();
            }else{
                myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                myfile << ("0," + std::to_string(currentInterval) + "\n");
                myfile.close(); 
            }
            */

           std::string str;

            if(symbolsTmp == 1){
                rMessageInBits.none();
                rLetterCounter = 0;
                messageToDecode.clear();
            } else if(currentInterval < 100){
                symbolsTmp = symbolsTmp / ALPHA;
                if(std::count(diffIntervals00.begin(), diffIntervals00.end(), symbolsTmp)){
                    str = diffChars2[0];
                } else if(std::count(diffIntervals01.begin(), diffIntervals01.end(), symbolsTmp)){
                    str = diffChars2[1];
                } else if(std::count(diffIntervals10.begin(), diffIntervals10.end(), symbolsTmp)){
                    str = diffChars2[2];
                } else if(std::count(diffIntervals11.begin(), diffIntervals11.end(), symbolsTmp)){
                    str = diffChars2[3];
                } else {
                    std::cout << "CURRENT_INT - " << currentInterval << std::endl;
                    std::cout << "SYMBOLSTEMP - " << symbolsTmp << std::endl;
                    DSME_SIM_ASSERT(false);
                }
                myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                myfile << (std::to_string(currentInterval) + ",0" + "\n");
                myfile.close();
                //messageTemp = diffChars[symbolsTmp-3];
                //LOG_DEBUG(diffChars[symbolsTmp-3]);
                LOG_DEBUG("COUNTER: " << rBitCounter);
                LOG_DEBUG("MESSAGE TEMP: " << str);
                std::string charMessageTmp = str;
                //if(rBitCounter >= 8){
                    //messageTemp.erase(std::remove_if(messageTemp.begin(), messageTemp.end(), ::isspace), messageTemp.end());
                    messageToDecode+=charMessageTmp;
                    rLetterCounter++;
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG("CHAR: " << messageTemp);
                    LOG_DEBUG("MESSAGE:" << messageToDecode);
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                //}
            } else if(currentInterval < 30000) {
                myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                myfile << ("0," + std::to_string(currentInterval) + "\n");
                myfile.close(); 
            }
            
            lastPacketTime = millis;
            std::cout << " " << std::endl;
        }
    }

    void CovertHelperReceiver::readCovert2ListsExp(mcps_sap::DATA_indication_parameters& params){

        // 0ms <= 0 <= 6ms <= 1 <= 9ms <= nothing

        /*using std::chrono::system_clock;
        using std::chrono::duration_cast;
        using std::chrono::milliseconds;
        using std::chrono::system_clock;*/
                    
        //auto millis = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();

        uint32_t diffIntervals[BYTE_NUM][LIST_SIZE];
        std::vector<std::string> possibleCombinations {
            "0000",
            "0001",
            "0010",
            "0011",
            "0100",
            "0101",
            "0110",
            "0111",
            "1000",
            "1001",
            "1010",
            "1011",
            "1100",
            "1101",
            "1110",
            "1111",
        };

        double millis = (double) omnetpp::simTime().dbl();
        LOG_DEBUG("ADDDDDRESSSSSSSSSSSS: " << params.msdu->getHeader().getSrcAddr().getShortAddress());
        LOG_DEBUG("ADDDDDRESSSSSSSSSSSSD: " << params.msdu->getHeader().getDestAddr().getShortAddress());
        LOG_DEBUG("LPT: " << lastPacketTime);
        LOG_DEBUG("SIZEEEEE: " << params.msdu->getTotalSymbols());
        uint8_t so = unsigned(this->dsmeAdaptionLayer.getMAC_PIB().macSuperframeOrder);
        uint16_t byteSize = (params.msdu->getTotalSymbols() - 120U) / 2;
        LOG_DEBUG("BYTESIZEEEEE: " << byteSize);
        std::ofstream myfile;

        // SYMBOLS:
        // base superframe duration = 960
        // base slot duration = 60
        // 1.5% of slot duration --> singular delay

        uint32_t symbols = const_redefines::macLIFSPeriod * 3;//60U * (pow(2, unsigned(so))) * 0.015;

        // TIME:
        // 960 symbols --> 15.36 ms
        // symbols symbols --> y ms
        // Then we add 3.104 ms that is the constant delay time when no delay is added to the packet and the packet length is 0B

        double time = ((symbols * 15.36) / 960) + (byteSize * 0.032) + 3.104;
        double time1 = (((symbols - const_redefines::macLIFSPeriod) * 15.36) / 960) + (byteSize * 0.032) + 3.104;
        LOG_DEBUG("PERFECT INTERVAL: " << time);
        LOG_DEBUG("PERFECT INTERVAL: " << time1);

        if(params.msdu->getHeader().getDestAddr().getShortAddress() == 1 && params.msdu->getHeader().getSrcAddr().getShortAddress() == 2){

            uint32_t counter = 2;

            for(int i=0; i<LIST_SIZE; i++){
                for(int j=0; j<BYTE_NUM; j++){
                    diffIntervals[j][i] = counter;
                    counter+=ALPHA;
                }
                
            }

            currentInterval = (millis - lastPacketTime) * 1000.0;

            uint32_t symbolsTmp = ((((currentInterval - 3.104 - (byteSize * 0.032)) * 960) / 15.36) + 0.5);// / const_redefines::macLIFSPeriod;

            LOG_DEBUG("AAAAAAAAAAAAAAAAAAAAAAAAAAA " << symbols << " " << symbolsTmp);
            LOG_DEBUG("SYMBOLS ACT: " << symbols);
            LOG_DEBUG("SYMBOLS PRED: " << symbolsTmp);

            LOG_DEBUG(omnetpp::simTime());
            LOG_DEBUG("LAST: " << lastPacketTime);
            LOG_DEBUG("NOW: " << millis);
            LOG_DEBUG("INTERVAL: " << currentInterval);

            std::cout << "INTERVAL - " << currentInterval << std::endl;
            std::cout << "BYTE SIZE - " << byteSize << std::endl;
            std::cout << "INTERVAL MINUS - " << ((((currentInterval - 3.104 - (byteSize * 0.032)) * 960) / 15.36)) << std::endl;

            std::string str;

            if(symbolsTmp == 1){
                rMessageInBits.none();
                rLetterCounter = 0;
                messageToDecode.clear();
            } else if(currentInterval < 100){

                bool found = false;
                int ind = 0;
                std::cout << "INTERVAL MOREUS - " << symbolsTmp << std::endl;
                for(int i=0; i<BYTE_NUM; i++){
                    uint32_t *foo = std::find(std::begin(diffIntervals[i]), std::end(diffIntervals[i]), symbolsTmp);
                    for(int j=0; j<LIST_SIZE; j++)
                        //std::cout << "LAAAAAAAAAAAAAAA - " << diffIntervals[i][j] << std::endl;
                    // When the element is not found, std::find returns the end of the range
                    if (foo != std::end(diffIntervals[i])) {
                        found = true;
                        std::cout << "   " << std::endl;
                        std::cout << "TRUEEEEEE" << std::endl;
                        ind = i;
                    }
                    std::cout << "   " << std::endl;
                }

                if(!found)
                    DSME_SIM_ASSERT(false);

                str = possibleCombinations.at(ind);

                /*if(std::count(diffIntervals00.begin(), diffIntervals00.end(), symbolsTmp)){
                    str = diffChars2[0];
                } else if(std::count(diffIntervals01.begin(), diffIntervals01.end(), symbolsTmp)){
                    str = diffChars2[1];
                } else if(std::count(diffIntervals10.begin(), diffIntervals10.end(), symbolsTmp)){
                    str = diffChars2[2];
                } else if(std::count(diffIntervals11.begin(), diffIntervals11.end(), symbolsTmp)){
                    str = diffChars2[3];
                } else {
                    std::cout << "CURRENT_INT - " << currentInterval << std::endl;
                    std::cout << "SYMBOLSTEMP - " << symbolsTmp << std::endl;
                    DSME_SIM_ASSERT(false);
                }*/

                myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                myfile << (std::to_string(currentInterval) + ",0" + "\n");
                myfile.close();
                //messageTemp = diffChars[symbolsTmp-3];
                //LOG_DEBUG(diffChars[symbolsTmp-3]);
                LOG_DEBUG("COUNTER: " << rBitCounter);
                LOG_DEBUG("MESSAGE TEMP: " << str);
                std::string charMessageTmp = str;
                //if(rBitCounter >= 8){
                    //messageTemp.erase(std::remove_if(messageTemp.begin(), messageTemp.end(), ::isspace), messageTemp.end());
                    messageToDecode+=charMessageTmp;
                    rLetterCounter++;
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG("CHAR: " << messageTemp);
                    LOG_DEBUG("MESSAGE:" << messageToDecode);
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                //}
            } else if(currentInterval < 40000) {
                myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                myfile << ("0," + std::to_string(currentInterval) + "\n");
                myfile.close(); 
            }
            
            lastPacketTime = millis;
            std::cout << " " << std::endl;
        }
    }

    void CovertHelperReceiver::readCovert2ListsExp8(mcps_sap::DATA_indication_parameters& params){

        // 0ms <= 0 <= 6ms <= 1 <= 9ms <= nothing

        /*using std::chrono::system_clock;
        using std::chrono::duration_cast;
        using std::chrono::milliseconds;
        using std::chrono::system_clock;*/
                    
        //auto millis = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();

        uint32_t diffIntervals[BYTE_NUM_8][LIST_SIZE];

        double millis = (double) omnetpp::simTime().dbl();
        LOG_DEBUG("ADDDDDRESSSSSSSSSSSS: " << params.msdu->getHeader().getSrcAddr().getShortAddress());
        LOG_DEBUG("ADDDDDRESSSSSSSSSSSSD: " << params.msdu->getHeader().getDestAddr().getShortAddress());
        LOG_DEBUG("LPT: " << lastPacketTime);
        LOG_DEBUG("SIZEEEEE: " << params.msdu->getTotalSymbols());
        uint8_t so = unsigned(this->dsmeAdaptionLayer.getMAC_PIB().macSuperframeOrder);
        uint16_t byteSize = (params.msdu->getTotalSymbols() - 120U) / 2;
        LOG_DEBUG("BYTESIZEEEEE: " << byteSize);
        std::ofstream myfile;

        // SYMBOLS:
        // base superframe duration = 960
        // base slot duration = 60
        // 1.5% of slot duration --> singular delay

        uint32_t symbols = const_redefines::macLIFSPeriod * 3;//60U * (pow(2, unsigned(so))) * 0.015;

        // TIME:
        // 960 symbols --> 15.36 ms
        // symbols symbols --> y ms
        // Then we add 3.104 ms that is the constant delay time when no delay is added to the packet and the packet length is 0B

        double time = ((symbols * 15.36) / 960) + (byteSize * 0.032) + 3.104;
        double time1 = (((symbols - const_redefines::macLIFSPeriod) * 15.36) / 960) + (byteSize * 0.032) + 3.104;
        LOG_DEBUG("PERFECT INTERVAL: " << time);
        LOG_DEBUG("PERFECT INTERVAL: " << time1);

        if(params.msdu->getHeader().getDestAddr().getShortAddress() == 1 && params.msdu->getHeader().getSrcAddr().getShortAddress() == 2){

            int counter = 2;

            for(int i=0; i<LIST_SIZE; i++){
                for(int j=0; j<BYTE_NUM_8; j++){
                    diffIntervals[j][i] = counter;
                    counter+=ALPHA;
                }
                
            }

            currentInterval = (millis - lastPacketTime) * 1000.0;

            uint32_t symbolsTmp = ((((currentInterval - 3.104 - (byteSize * 0.032)) * 960) / 15.36) + 0.5 );// / const_redefines::macLIFSPeriod;

            LOG_DEBUG("AAAAAAAAAAAAAAAAAAAAAAAAAAA " << symbols << " " << symbolsTmp);
            LOG_DEBUG("SYMBOLS ACT: " << symbols);
            LOG_DEBUG("SYMBOLS PRED: " << symbolsTmp);

            LOG_DEBUG(omnetpp::simTime());
            LOG_DEBUG("LAST: " << lastPacketTime);
            LOG_DEBUG("NOW: " << millis);
            LOG_DEBUG("INTERVAL: " << currentInterval);

            std::cout << "INTERVAL - " << currentInterval << std::endl;
            std::cout << "BYTE SIZE - " << byteSize << std::endl;
            std::cout << "INTERVAL MINUS - " << ((((currentInterval - 3.104 - (byteSize * 0.032)) * 960) / 15.36) + 0.5) << std::endl;
            std::cout << "INTERVAL MOREUS - " << symbolsTmp << std::endl;

            std::string str;

            if(symbolsTmp == 1){
                rMessageInBits.none();
                rLetterCounter = 0;
                messageToDecode.clear();
            } else if(currentInterval < 3000){

                bool found = false;
                int ind = 0;
                for(int i=0; i<BYTE_NUM_8; i++){
                    uint32_t *foo = std::find(std::begin(diffIntervals[i]), std::end(diffIntervals[i]), symbolsTmp);
                    for(int j=0; j<LIST_SIZE; j++)
                        //std::cout << "LAAAAAAAAAAAAAAA - " << diffIntervals[i][j] << std::endl;
                    // When the element is not found, std::find returns the end of the range
                    if (foo != std::end(diffIntervals[i])) {
                        found = true;
                        std::cout << "   " << std::endl;
                        std::cout << "TRUEEEEEE" << std::endl;
                        ind = i;
                    }
                }

                if(!found)
                    DSME_SIM_ASSERT(false);

                str = possibleCombinations8.at(ind);

                /*if(std::count(diffIntervals00.begin(), diffIntervals00.end(), symbolsTmp)){
                    str = diffChars2[0];
                } else if(std::count(diffIntervals01.begin(), diffIntervals01.end(), symbolsTmp)){
                    str = diffChars2[1];
                } else if(std::count(diffIntervals10.begin(), diffIntervals10.end(), symbolsTmp)){
                    str = diffChars2[2];
                } else if(std::count(diffIntervals11.begin(), diffIntervals11.end(), symbolsTmp)){
                    str = diffChars2[3];
                } else {
                    std::cout << "CURRENT_INT - " << currentInterval << std::endl;
                    std::cout << "SYMBOLSTEMP - " << symbolsTmp << std::endl;
                    DSME_SIM_ASSERT(false);
                }*/

                myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                myfile << (std::to_string(currentInterval) + ",0" + "\n");
                myfile.close();
                //messageTemp = diffChars[symbolsTmp-3];
                //LOG_DEBUG(diffChars[symbolsTmp-3]);
                LOG_DEBUG("COUNTER: " << rBitCounter);
                LOG_DEBUG("MESSAGE TEMP: " << str);
                std::string charMessageTmp = str;
                //if(rBitCounter >= 8){
                    //messageTemp.erase(std::remove_if(messageTemp.begin(), messageTemp.end(), ::isspace), messageTemp.end());
                    messageToDecode+=charMessageTmp;
                    rLetterCounter++;
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG("CHAR: " << messageTemp);
                    LOG_DEBUG("MESSAGE:" << messageToDecode);
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                //}
            } else if(currentInterval < 40000) {
                myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                myfile << ("0," + std::to_string(currentInterval) + "\n");
                myfile.close(); 
            }
            
            lastPacketTime = millis;
            std::cout << " " << std::endl;
        }
    }

    // The main recursive method
    // to print all possible
    // strings of length k
    void CovertHelperReceiver::printAllKLengthRec(char set[], std::string prefix,
                                        int n, int k)
    {
        
        // Base case: k is 0,
        // print prefix
        if (k == 0)
        {
            possibleCombinations8.push_back(prefix);
            return;
        }
    
        // One by one add all characters
        // from set and recursively
        // call for k equals to k-1
        for (int i = 0; i < n; i++)
        {
            std::string newPrefix;
            
            // Next character of input added
            newPrefix = prefix + set[i];
            
            // k is decreased, because
            // we have added a new character
            printAllKLengthRec(set, newPrefix, n, k - 1);
        }
    
    }
    
    void CovertHelperReceiver::printAllKLength(char set[], int k, int n)
    {
        printAllKLengthRec(set, "", n, k);
    }

    std::string CovertHelperReceiver::decodePayload(std::string msg, std::string key){
        std::string finalMsg;
        for(int i=0; i < msg.length(); i += key.length()){
            std::string tmp = msg.substr(i, i + key.length());
            for(int j=0; j < key.length(); j++)
                finalMsg += tmp[j] ^ key[j];
        }
        return finalMsg;
    }

    std::vector<std::string> split(std::string &s, char delim){
        std::stringstream ss(s);
        std::string item;
        std::vector<std::string> res;
        while(std::getline(ss, item, delim)) {
            res.push_back(std::move(item));
        }
        return res;
    }

    void CovertHelperReceiver::readCovertPayload(mcps_sap::DATA_indication_parameters& params){

        // 0ms <= 0 <= 6ms <= 1 <= 9ms <= nothing

        /*using std::chrono::system_clock;
        using std::chrono::duration_cast;
        using std::chrono::milliseconds;
        using std::chrono::system_clock;*/
                    
        //auto millis = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();

        diffChars.clear();
        diffChars.push_back('a');
        diffChars.push_back('b');
        diffChars.push_back('c');
        diffChars.push_back(',');

        double millis = (double) omnetpp::simTime().dbl();
        LOG_DEBUG("ADDDDDRESSSSSSSSSSSS: " << params.msdu->getHeader().getSrcAddr().getShortAddress());
        LOG_DEBUG("ADDDDDRESSSSSSSSSSSSD: " << params.msdu->getHeader().getDestAddr().getShortAddress());
        LOG_DEBUG("LPT: " << lastPacketTime);
        LOG_DEBUG("SIZEEEEE: " << params.msdu->getTotalSymbols());
        uint8_t so = unsigned(this->dsmeAdaptionLayer.getMAC_PIB().macSuperframeOrder);
        uint16_t byteSize = (params.msdu->getTotalSymbols() - 120U) / 2;
        LOG_DEBUG("BYTESIZEEEEE: " << byteSize);
        std::ofstream myfile;

        // SYMBOLS:
        // base superframe duration = 960
        // base slot duration = 60
        // 1.5% of slot duration --> singular delay

        uint32_t symbols = const_redefines::macLIFSPeriod * 3;//60U * (pow(2, unsigned(so))) * 0.015;

        // TIME:
        // 960 symbols --> 15.36 ms
        // symbols symbols --> y ms
        // Then we add 3.104 ms that is the constant delay time when no delay is added to the packet and the packet length is 0B

        double time = ((symbols * 15.36) / 960) + (byteSize * 0.032) + 3.104;
        double time1 = (((symbols - const_redefines::macLIFSPeriod) * 15.36) / 960) + (byteSize * 0.032) + 3.104;
        LOG_DEBUG("PERFECT INTERVAL: " << time);
        LOG_DEBUG("PERFECT INTERVAL: " << time1);

        if(params.msdu->getHeader().getDestAddr().getShortAddress() == 1 && params.msdu->getHeader().getSrcAddr().getShortAddress() == 2){
            currentInterval = (millis - lastPacketTime) * 1000.0;


            int16_t symbolsTmp = ((((currentInterval - 3.104 - (byteSize * 0.032)) * 960) / 15.36) + 0.5) / const_redefines::macLIFSPeriod;

            LOG_DEBUG("AAAAAAAAAAAAAAAAAAAAAAAAAAA " << symbols << " " << symbolsTmp);
            LOG_DEBUG("SYMBOLS ACT: " << symbols);
            LOG_DEBUG("SYMBOLS PRED: " << symbolsTmp);

            LOG_DEBUG(omnetpp::simTime());
            LOG_DEBUG("LAST: " << lastPacketTime);
            LOG_DEBUG("NOW: " << millis);
            LOG_DEBUG("INTERVAL: " << currentInterval);

            if(symbolsTmp == 1){
                rMessageInBits.none();
                rLetterCounter = 0;
                messageToDecode.clear();
                messageToDecode2.clear();
            } else if(currentInterval < 100){
                myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                myfile << (std::to_string(currentInterval) + ",0" + "\n");
                myfile.close();
                messageTemp = diffChars[symbolsTmp-2];
                LOG_DEBUG(diffChars[symbolsTmp-2]);
                LOG_DEBUG("COUNTER: " << symbolsTmp);
                LOG_DEBUG("MESSAGE TEMP: " << messageTemp);
                char charMessageTmp = messageTemp;
                for(uint8_t i: params.msdu->getPayloadData(byteSize)){
                    if(i == ','){
                        messageDecoded += decodePayload(messageToDecode2, split(messageToDecode, ',')[1]);
                        messageToDecode2.clear();
                        messageToDecode.clear();
                    } else {
                        messageToDecode2.append(1, i);
                        messageToDecode.append(1, charMessageTmp);
                    }
                }
                rLetterCounter++;
                LOG_DEBUG(" ");
                LOG_DEBUG(" ");
                LOG_DEBUG(" ");
                LOG_DEBUG(" ");
                LOG_DEBUG(" ");
                LOG_DEBUG(" ");
                LOG_DEBUG(" ");
                LOG_DEBUG(" ");
                LOG_DEBUG(" ");
                LOG_DEBUG("CHAR: " << messageTemp);
                LOG_DEBUG("MESSAGE:" << messageToDecode);
                for(uint8_t i: params.msdu->getPayloadData(byteSize)){
                    LOG_DEBUG(" ");
                    LOG_DEBUG("DIFF_CHARS: " << byteSize);
                    LOG_DEBUG("PAYLOAD: " << i);
                    LOG_DEBUG("PAYLOAD2: " << unsigned(i));
                    LOG_DEBUG("MESSAGE_DEC: " << messageToDecode2);
                    LOG_DEBUG("MESSAGE_DECRYPTED: " << messageDecoded);
                }
                LOG_DEBUG(" ");
                LOG_DEBUG(" ");
                LOG_DEBUG(" ");
                LOG_DEBUG(" ");
                LOG_DEBUG(" ");
                LOG_DEBUG(" ");
                LOG_DEBUG(" ");
                LOG_DEBUG(" ");
                LOG_DEBUG(" ");
            } else if(currentInterval < 30000) {
                myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                myfile << ("0," + std::to_string(currentInterval) + "\n");
                myfile.close(); 
            }
            
            lastPacketTime = millis;
        }
    }
    


    void CovertHelperReceiver::autentication(mcps_sap::DATA_indication_parameters& params) { // 1 bit
        double millis = (double) omnetpp::simTime().dbl(); // obter o tempo do omnett
        
        uint32_t diffIntervals[Bits_1][Times];// criação de uma matriz para saber ao delay que correspondem os bits

        std::vector<std::string> possibleCombinations { //possiveis combinações dos delays
            "0",
            "1",
            
            
            
            
        };
        
        currentInterval = (millis - lastPacketTime) * 1000.0;
        LOG_DEBUG("currentInterval: " << currentInterval);
            std::cout << "INTERVAL - " << currentInterval << std::endl;
        uint16_t byteSize = (params.msdu->getTotalSymbols() - 120U) / 2;
        LOG_DEBUG("byteSize: " << byteSize);
        uint32_t symbolsTmp = ((((currentInterval - 3.104 - (byteSize * 0.032)) * 960) / 15.36) + 0.5 );// buscar o delay
        LOG_DEBUG("Tempo_da_mensagem" << symbolsTmp);
        double Delaymsgms = (symbolsTmp * 15.36)/960;
        


       if(params.msdu->getHeader().getDestAddr().getShortAddress() == 1 && params.msdu->getHeader().getSrcAddr().getShortAddress() == 2){

            int counter = 2;

            for(int i=0; i<Times; i++){
                for(int j=0; j<Bits_1; j++){
                    diffIntervals[j][i] = counter;
                    counter+=ALPHA;
                }
            }
            if(symbolsTmp == 1){
                rMessageInBits.none();
                rLetterCounter = 0;
                messageToDecode.clear();
            }else if(currentInterval < 400){

                vetorTempPrg.push_back(Delaymsgms);

            


            std::string str;

            bool found = false;
            int ind = 0;
            std::cout << "INTERVAL MOREUS - " << symbolsTmp << std::endl;
            for(int i=0; i<Bits_1; i++){
                uint32_t *foo = std::find(std::begin(diffIntervals[i]), std::end(diffIntervals[i]), symbolsTmp);
                for(int j=0; j<Times; j++)
                    std::cout << "LAAAAAAAAAAAAAAA - " << diffIntervals[i][j] << std::endl;
                // When the element is not found, std::find returns the end of the range
                if (foo != std::end(diffIntervals[i])) {
                    found = true;
                    std::cout << "   " << std::endl;
                    std::cout << "TRUEEEEEE" << std::endl;
                    ind = i;
                }
                std::cout << "   " << std::endl;
            }

            if(!found)
                DSME_SIM_ASSERT(false);
        

            str = possibleCombinations.at(ind);



            LOG_DEBUG("Counter: " << rBitCounter);
            LOG_DEBUG("MESSAGE TEMP: " << str);
            std::string charMessageTmp = str;
                //if(rBitCounter >= 8){
                    //messageTemp.erase(std::remove_if(messageTemp.begin(), messageTemp.end(), ::isspace), messageTemp.end());
                    messageAuthToDecode+=charMessageTmp;
                    rLetterCounter++;
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG("CHAR: " << messageTemp);
                    LOG_DEBUG("MESSAGE:" << messageAuthToDecode);
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");

                    
        
            

             if(messageAuthToDecode.size() >= numero){

                std::cout << messageAuthToDecode << std::endl;
                    
               if(messageAuthToDecode.compare((diffCodes[params.msdu->getHeader().getSrcAddr().getShortAddress()-1][0]) + diffCodes[params.msdu->getHeader().getDestAddr().getShortAddress()-1][0])  == 0 ){
               Autentication = 1;
               LOG_INFO(Autentication);
               LOG_INFO("Sucessful Authentication");
               Cont_Aut++;
               LOG_INFO("Numero de Autenticações = " << Cont_Aut);
               }else
               {
                Autentication = 2;
                LOG_INFO("Chave de autenticação errada");
               }

            messageAuthToDecode.clear();   



        }
            
            
            
            
            
            
            
            
            
            
            
            
           }lastPacketTime = millis;
        }
    }

     void CovertHelperReceiver::autentication2(mcps_sap::DATA_indication_parameters& params) { // 2 bits
        double millis = (double) omnetpp::simTime().dbl(); // obter o tempo do omnett
       
        uint32_t diffIntervals[Bits_2][Times];// criação de uma matriz para saber ao delay que correspondem os bits

        std::vector<std::string> possibleCombinations { //possiveis combinações dos delays
            "00",
            "01",
            "10",
            "11",
            
            
        };
        
        currentInterval = (millis - lastPacketTime) * 1000.0;
        LOG_DEBUG("currentInterval: " << currentInterval);
        uint16_t byteSize = (params.msdu->getTotalSymbols() - 120U) / 2;
        LOG_DEBUG("byteSize: " << byteSize);
        uint32_t symbolsTmp = ((((currentInterval - 3.104 - (byteSize * 0.032)) * 960) / 15.36) + 0.5 );// buscar o delay
        LOG_DEBUG("Tempo_da_mensagem " << symbolsTmp);
        double Delaymsgms = (symbolsTmp * 15.36)/960;
        


       if(params.msdu->getHeader().getDestAddr().getShortAddress() == 1 && params.msdu->getHeader().getSrcAddr().getShortAddress() == 2){

            int counter = 2;

            for(int i=0; i<Times; i++){
                for(int j=0; j<Bits_2; j++){
                    diffIntervals[j][i] = counter;
                    counter+=ALPHA;
                }
            }
            if(symbolsTmp == 1){
                rMessageInBits.none();
                rLetterCounter = 0;
                messageToDecode.clear();
            }else if(currentInterval < 100){

                vetorTempPrg.push_back(Delaymsgms);

            


            std::string str;

            bool found = false;
            int ind = 0;
            std::cout << "INTERVAL MOREUS - " << symbolsTmp << std::endl;
            for(int i=0; i<Bits_2; i++){
                uint32_t *foo = std::find(std::begin(diffIntervals[i]), std::end(diffIntervals[i]), symbolsTmp); // LIsta de possiveis intervalos e compara com delays e vai buscar a combinação
                for(int j=0; j<Times; j++)
                    //std::cout << "LAAAAAAAAAAAAAAA - " << diffIntervals[i][j] << std::endl;
                // When the element is not found, std::find returns the end of the range
                if (foo != std::end(diffIntervals[i])) {
                    found = true;
                    std::cout << "   " << std::endl;
                    std::cout << "TRUEEEEEE" << std::endl;
                    ind = i;
                }
                std::cout << "   " << std::endl;
            }

            if(!found)
                DSME_SIM_ASSERT(false);
        

            str = possibleCombinations.at(ind);



            LOG_DEBUG("Counter: " << rBitCounter);
            LOG_DEBUG("MESSAGE TEMP: " << str);
            std::string charMessageTmp = str;
                //if(rBitCounter >= 8){
                    //messageTemp.erase(std::remove_if(messageTemp.begin(), messageTemp.end(), ::isspace), messageTemp.end());
                    messageAuthToDecode+=charMessageTmp; 
                    rLetterCounter++;
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG("CHAR: " << messageTemp);
                    LOG_DEBUG("MESSAGE:" << messageAuthToDecode);
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");

                    
        
            

            if(messageAuthToDecode.size() >= numero){

                std::cout << messageAuthToDecode << std::endl;
                    
               if(messageAuthToDecode.compare((diffCodes[params.msdu->getHeader().getSrcAddr().getShortAddress()-1][0]) + diffCodes[params.msdu->getHeader().getDestAddr().getShortAddress()-1][0])  == 0 ){
               Autentication = 1;
               LOG_INFO(Autentication);
               LOG_INFO("Autenticação bem sucedida");
               Cont_Aut++;
               LOG_INFO("Sucessful Authentication = " << Cont_Aut);
               }else
               {
                Autentication = 2;
                LOG_INFO("Chave de autenticação errada");
               }

            messageAuthToDecode.clear();   



        }
            
            
            
            
            
            
            
            
            
            
           }lastPacketTime = millis;
        }
    } 



     void CovertHelperReceiver::autentication4(mcps_sap::DATA_indication_parameters& params) { // 4 bits
        double millis = (double) omnetpp::simTime().dbl(); // obter o tempo do omnett
        
        
        uint32_t diffIntervals[Bits][Times];// matrix with bits and delays

        std::vector<std::string> possibleCombinations { //possible bits combination
            "0000",
            "0001",
            "0010",
            "0011",
            "0100",
            "0101",
            "0110",
            "0111",
            "1000",
            "1001",
            "1010",
            "1011",
            "1100",
            "1101",
            "1110",
            "1111",
            
        };
        
        currentInterval = (millis - lastPacketTime) * 1000.0;
        LOG_DEBUG("currentInterval: " << currentInterval);
        uint16_t byteSize = (params.msdu->getTotalSymbols() - 120U) / 2;
        LOG_DEBUG("byteSize: " << byteSize);
        uint32_t symbolsTmp = ((((currentInterval - 3.104 - (byteSize * 0.032)) * 960) / 15.36) + 0.5 );// buscar o delay
        LOG_DEBUG("Tempo_da_mensagem: " << symbolsTmp);
        double Delaymsgms = (symbolsTmp * 15.36)/960;
        


       if(params.msdu->getHeader().getDestAddr().getShortAddress() == 1 && params.msdu->getHeader().getSrcAddr().getShortAddress() == 2){

            int counter = 2;

            for(int i=0; i<Times; i++){
                for(int j=0; j<Bits; j++){
                    diffIntervals[j][i] = counter;
                    counter+=ALPHA;
                }
            }
            if(symbolsTmp == 1){
                rMessageInBits.none();
                rLetterCounter = 0;
                messageToDecode.clear();
            }else if(currentInterval < 100){

                vetorTempPrg.push_back(Delaymsgms);

            


            std::string str;

            bool found = false;
            int ind = 0;
            std::cout << "INTERVAL MOREUS - " << symbolsTmp << std::endl;
            for(int i=0; i<Bits; i++){
                uint32_t *foo = std::find(std::begin(diffIntervals[i]), std::end(diffIntervals[i]), symbolsTmp);
                for(int j=0; j<Times; j++)
                    //std::cout << "LAAAAAAAAAAAAAAA - " << diffIntervals[i][j] << std::endl;
                // When the element is not found, std::find returns the end of the range
                if (foo != std::end(diffIntervals[i])) {
                    found = true;
                    std::cout << "   " << std::endl;
                    std::cout << "TRUEEEEEE" << std::endl;
                    ind = i;
                }
                std::cout << "   " << std::endl;
            }

            if(!found)
                DSME_SIM_ASSERT(false);
        

            str = possibleCombinations.at(ind);



            LOG_DEBUG("Counter: " << rBitCounter);
            LOG_DEBUG("MESSAGE TEMP: " << str);
            std::string charMessageTmp = str;
                //if(rBitCounter >= 8){
                    //messageTemp.erase(std::remove_if(messageTemp.begin(), messageTemp.end(), ::isspace), messageTemp.end());
                    messageAuthToDecode+=charMessageTmp;
                    rLetterCounter++;
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG("CHAR: " << messageTemp);
                    LOG_DEBUG("MESSAGE:" << messageAuthToDecode);
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");

                    
        
            

            if(messageAuthToDecode.size() >= numero){
                    
               if(messageAuthToDecode.compare((diffCodes[params.msdu->getHeader().getSrcAddr().getShortAddress()-1][0]) + diffCodes[params.msdu->getHeader().getDestAddr().getShortAddress()-1][0])  == 0 ){
               Autentication = 1;
               LOG_INFO(Autentication);
               LOG_INFO("Sucessful Authentication");
               Cont_Aut++;
               LOG_INFO("Numero de Autenticações = " << Cont_Aut);
               }else
               {
                Autentication = 2;
                LOG_INFO("Chave de autenticação errada");
               }

            messageAuthToDecode.clear();   



        }
            
            
            
            
            
            
            
            
            
            
           }lastPacketTime = millis;
        }
    }



    void CovertHelperReceiver::autentication8(mcps_sap::DATA_indication_parameters& params) { // 8 bits
        double millis = (double) omnetpp::simTime().dbl(); // obter o tempo do omnett
        
        uint32_t diffIntervals[BYTE_NUM_8][Times];// criação de uma matriz para saber ao delay que correspondem os bits

        
        
        currentInterval = (millis - lastPacketTime) * 1000.0;
        LOG_DEBUG("currentInterval: " << currentInterval);
        uint16_t byteSize = (params.msdu->getTotalSymbols() - 120U) / 2;
        LOG_DEBUG("byteSize: " << byteSize);
        uint32_t symbolsTmp = ((((currentInterval - 3.104 - (byteSize * 0.032)) * 960) / 15.36) + 0.5 );// buscar o delay
        // 960 symbols --> 15.36 ms
        double Delaymsgms = (symbolsTmp * 15.36)/960;
        LOG_DEBUG("Tempo_da_mensagem: " << symbolsTmp);
        LOG_DEBUG("Tempo_da_mensagem em ms: " << Delaymsgms);

       
        
        


       if(params.msdu->getHeader().getDestAddr().getShortAddress() == 1 && params.msdu->getHeader().getSrcAddr().getShortAddress() == 2){

            int counter = 2;

            for(int i=0; i<Times; i++){
                for(int j=0; j<BYTE_NUM_8; j++){
                    diffIntervals[j][i] = counter;
                    counter+=ALPHA;
                }
            }
            if(symbolsTmp == 1){
                rMessageInBits.none();
                rLetterCounter = 0;
                messageToDecode.clear();
            }else if(currentInterval < 100){

            
            vetorTempPrg.push_back(Delaymsgms);
            end++;

            std::string str;

            bool found = false;
            int ind = 0;
            std::cout << "INTERVAL MOREUS - " << symbolsTmp << std::endl;
            for(int i=0; i<BYTE_NUM_8; i++){
                uint32_t *foo = std::find(std::begin(diffIntervals[i]), std::end(diffIntervals[i]), symbolsTmp);
                for(int j=0; j<Times; j++)
                    //std::cout << "LAAAAAAAAAAAAAAA - " << diffIntervals[i][j] << std::endl;
                // When the element is not found, std::find returns the end of the range
                if (foo != std::end(diffIntervals[i])) {
                    found = true;
                    std::cout << "   " << std::endl;
                    std::cout << "TRUEEEEEE" << std::endl;
                    ind = i;
                }
                std::cout << "   " << std::endl;
            }

            if(!found)
                DSME_SIM_ASSERT(false);
        

            str = possibleCombinations8.at(ind);



            LOG_DEBUG("Counter: " << rBitCounter);
            LOG_DEBUG("MESSAGE TEMP: " << str);
            std::string charMessageTmp = str;
                //if(rBitCounter >= 8){
                    //messageTemp.erase(std::remove_if(messageTemp.begin(), messageTemp.end(), ::isspace), messageTemp.end());
                    messageAuthToDecode+=charMessageTmp;
                    rLetterCounter++;
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG("CHAR: " << messageTemp);
                    LOG_DEBUG("Authentication Key transmitted : " << messageAuthToDecode);
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");
                    LOG_DEBUG(" ");

                    
        
            

            if(messageAuthToDecode.size() >= numero){
                    
               if(messageAuthToDecode.compare((diffCodes[params.msdu->getHeader().getSrcAddr().getShortAddress()-1][0]) + diffCodes[params.msdu->getHeader().getDestAddr().getShortAddress()-1][0])  == 0 ){
               Autentication = 1;
               LOG_INFO(Autentication);
               LOG_INFO("Sucessful Authentication");
               Cont_Aut++;
               LOG_INFO("Autentication Number= " << Cont_Aut);
               }else
               {
                Autentication = 2;
                LOG_INFO("Chave de autenticação errada");
               }

            messageAuthToDecode.clear();            



        }
            
            
            
            
            
            
            
            
            
            
           }lastPacketTime = millis;
        }
    }

    int CovertHelperReceiver::getAuthentication(){
        return Autentication;
    }

    void CovertHelperReceiver::resetAuthentication(){
        Autentication = 0;
    }

    uint8_t CovertHelperReceiver::gettechAutBytes(){
    return techAutBytes;
}
    std::vector<double> CovertHelperReceiver::getvetorTempPrg(){
        return vetorTempPrg;
    }
    void CovertHelperReceiver::resetvetorTempPrg(){
        vetorTempPrg.clear();

    }

    int CovertHelperReceiver::getKeysize(){
        return numero;
    }

    int CovertHelperReceiver::getCont(){

        return Cont_Aut;

    }
    //int CovertHelperReceiver::getend(){
        //return end;
    //}

    
} 

