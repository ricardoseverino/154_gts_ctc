#ifndef COVERTHELPERRECEIVER_H_
#define COVERTHELPERRECEIVER_H_

#include <string>
#include <bitset>
#include <vector>

#define LIST_SIZE (3)
#define BYTE_NUM (16)
#define ALPHA (1)
#define Bits_2 (4)
#define Bits_1 (2)
#define Nodes (2)
#define Codes (1)

namespace dsme {

class DSMEAdaptionLayer;

namespace mcps_sap {
class MCPS_SAP;
struct DATA_confirm_parameters;
struct DATA_indication_parameters;
} 

class CovertHelperReceiver{
public:
    explicit CovertHelperReceiver(DSMEAdaptionLayer& dsmeAdaptionLayer);

    void initialize(void);

    char getCharFromBitset(std::bitset<8> s);

    void initializeConfigs(std::string tech, uint8_t bytes, uint8_t techAutBytes);

    void readCovertDelay(mcps_sap::DATA_indication_parameters& params);

    void readCovertOnOff(mcps_sap::DATA_indication_parameters& params);

    void autentication(mcps_sap::DATA_indication_parameters& params);

    void autentication2(mcps_sap::DATA_indication_parameters& params);

    void autentication4(mcps_sap::DATA_indication_parameters& params);

    void autentication8(mcps_sap::DATA_indication_parameters& params);

    void readCovertList(mcps_sap::DATA_indication_parameters& params);

    void readCovertListExp(mcps_sap::DATA_indication_parameters& params);

    void readCovertListExp8(mcps_sap::DATA_indication_parameters& params);

    void readCovert2Lists(mcps_sap::DATA_indication_parameters& params);

    void readCovert2ListsExp(mcps_sap::DATA_indication_parameters& params);

    void readCovert2ListsExp8(mcps_sap::DATA_indication_parameters& params);

    void readCovertPayload(mcps_sap::DATA_indication_parameters& params);

    void printAllKLengthRec(char set[], std::string prefix, int n, int k);

    void printAllKLength(char set[], int k,int n);

    int getKeyLength();

    int getAuthentication();

    int getKeysize();

    int getCont();

    uint8_t gettechAutBytes();

    std::vector<double> getvetorTempPrg();

    void resetvetorTempPrg();
    int getend();



    void resetAuthentication();

    std::string decodePayload(std::string msg, std::string key);


private:

    DSMEAdaptionLayer& dsmeAdaptionLayer;

    std::string messageToDecode;
    std::string messageToDecode2;
    std::string messageDecoded;
    std::string messageAuthToDecode;
    std::bitset<8> rMessageInBits;
    char messageTemp;
    int Autentication = 0;
    int numero = 32;
    int Cont_Aut = 0;
    size_t rBitCounter = 0;
    int rLetterCounter = 0;
    double lastPacketTime = 0;
    int end = 0;

    double currentInterval = 0;
    std::vector<char> diffChars {'0','1'};
    std::vector<std::string> diffChars2 {"00", "01", "10", "11"};
    std::string diffCodes[Nodes][Codes];
    std::vector<std::string> possibleCombinations8;

    std::string tech;
    uint8_t techBytes;
    uint8_t techAutBytes;
    std::vector<double> vetorTempPrg{};

};

}

#endif