#ifndef COVERTHELPER_H_
#define COVERTHELPER_H_

#include "../../helper/Integers.h"
#include "../../interfaces/IDSMEPlatform.h"
#include "../TimerAbstractions.h"
//#include "../interfaces/IDSMEMessage.h"
#include <string>
#include <bitset>
#include <vector>

#define LIST_SIZE (3)
#define BYTE_NUM (16)
#define BYTE_NUM_8 (255)
#define ALPHA (1)
#define Nodes (2)
#define Codes (1)
#define Bits (16)
#define Times (3)
#define Bits_2 (4)
#define Bits_1 (2)

namespace dsme {

class DSMELayer;

class CovertHelper{
public:
    explicit CovertHelper(DSMELayer& dsme);

    void initialize(void);
    /*! Method to convert a char to a bitset
     *
     */
    std::bitset<8> getBitsetFromChar(char s);

    /*! Define covert channel configurations 
     *
     */
    void initializeConfigs(std::string tech, uint8_t bytes, uint8_t techAutBytes, std::string message);

    /*! Insert delay of desired kind 
     *
     */
    void insertDelay(IDSMEMessage* msg);

    /*! Insert delay OnOff in transmission
     *
     */
    void insertDelayOnOff(IDSMEMessage* msg);

    /*! Insert delay List in transmission
     *
     */
    void insertDelayList(IDSMEMessage* msg);

    /*! Autentication process
     *
     */

    void autentication(IDSMEMessage* msg);

    void autentication2(IDSMEMessage* msg);

    void autentication4(IDSMEMessage* msg);

    void autentication8(IDSMEMessage* msg);

    /*! Insert delay List in transmission
     *
     */
    void insertDelayListExp(IDSMEMessage* msg);

    /*! Insert delay List in transmission
     *
     */
    void insertDelayListExp8(IDSMEMessage* msg);

    /*! Insert delay 2 Lists in transmission
     *
     */
    void insertDelay2Lists(IDSMEMessage* msg);

    /*! Insert delay 2 Lists in transmission (Experimental)
     *
     */
    void insertDelay2ListsExp(IDSMEMessage* msg);

    /*! Insert delay 2 Lists in transmission (Experimental)
     *
     */
    void insertDelay2ListsExp8(IDSMEMessage* msg);

    /*! Insert delay with payload mods (god help us)
     *
     */
    void insertDelayPayload(IDSMEMessage* msg);

    void printAllKLengthRec(char set[], std::string prefix, int n, int k);

    void printAllKLength(char set[], int k,int n);

    /*! Insert delay with payload mods (god help us)
     *
     */
    IDSMEMessage* modifyPayload(IDSMEMessage* msg);

    /*! Insert delay with payload mods (god help us)
     *
     */
    std::string encodeMessage(std::string msg, std::string key);

    /*! Insert delay with payload mods (god help us)
     *
     */
    char symbolToSend();

private:
    DSMELayer& dsme;
    ReadonlyTimerAbstraction<IDSMEPlatform> NOW;

    enum AckLayerResponse lastResponse;
    IDSMEMessage* lastMessage;

    std::vector<uint8_t> diffIntervals {0,static_cast<unsigned char>(2+ALPHA)};
    std::vector<char> diffChars {'0','1'};
    std::string messageToEncode;
    std::string messageToEncodeInPayload;
    std::string messageEncodedToSendInPayload;
    std::bitset<8> messageInBits;
    std::string tech;
    uint8_t techBytes;
    uint8_t techAutBytes;
    size_t bitCounter = 0;
    int letterCounter = 0;
    bool Autentication = false;
    bool first = true;
    int numero = 32;
    double lastTime = 0.0;
    uint32_t allPackets = 0;
    uint16_t covertPackets = 0;
    uint16_t covertPacketsCounter = 0;
    uint16_t superframes = 0;
    uint16_t bitssent = 0;
    uint8_t bitCounterEncode = 0;
    uint8_t currentSymbol = 0;

    std::string diffCodes[Nodes][Codes];

    std::vector<std::string> possibleCombinations8;
};

}

#endif