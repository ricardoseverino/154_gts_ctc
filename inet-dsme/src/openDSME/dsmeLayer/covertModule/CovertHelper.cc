#include "./CovertHelper.h"

#include "../../dsmeLayer/DSMELayer.h"
#include "../../interfaces/IDSMEMessage.h"
#include <omnetpp.h>
#include <math.h>
#include <chrono>
#include <thread>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <random>
#include <algorithm>

namespace dsme {

    CovertHelper::CovertHelper(DSMELayer& dsme)
        : dsme(dsme){

    }

    void CovertHelper::initialize(void) {
        this->NOW.initialize(&(this->dsme.getPlatform()), &IDSMEPlatform::getSymbolCounter);
        messageToEncode = "0110100101101110011001100110111101110010011011010110000101100011011000010110111100100000011000110110111101101110011001100110100101100100011001010110111001100011011010010110000101101100001000000010000000100000";
        //messageInBits = getBitsetFromChar(messageToEncode[letterCounter]);
        diffCodes[0][0] = "0000011011001000";
        diffCodes[1][0] = "0000011011001000";
        std::ofstream myfile;
        uint8_t so = unsigned(this->dsme.getMAC_PIB().macSuperframeOrder);
        if(unsigned(this->dsme.getPlatform().getSrcAddress()) == 2){
            char possibleBits[] = {'0', '1'};
            printAllKLength(possibleBits, 8, 2);
            myfile.open ("results/writer" + std::to_string(so) + ".csv");
            myfile << "Time,CovertPackets,AllPackets,BitsSent,PacketInterval,GTSInterval\n";
            myfile.close();
            myfile.open ("tmp.csv");
            myfile << std::to_string(allPackets);
            myfile.close();
           

        }
        return;
    }

    void CovertHelper::initializeConfigs(std::string tech_new, uint8_t techBytes_new, uint8_t techAutBytes_new, std::string message){
        messageToEncode = message;
        tech = tech_new;
        techBytes = techBytes_new;
        techAutBytes = techAutBytes_new;
        uint8_t so = unsigned(this->dsme.getMAC_PIB().macSuperframeOrder);
        
        tech.erase(std::remove(tech.begin(), tech.end(), '\"'), tech.end());
        messageToEncode.erase(std::remove(messageToEncode.begin(), messageToEncode.end(), '\"'), messageToEncode.end());
        std::ofstream myfile;
        
         myfile.open ("Joao/NDelayTeste_SO" + std::to_string(so) + "_" + std::to_string(techAutBytes) + "_" + std::to_string(numero) + ".csv");
            myfile << "Delay,Aut_Number,Aut_Slot,MensagemAut\n";
            myfile.close();
    }

     void CovertHelper::insertDelay(IDSMEMessage* msg){
        if (Autentication == false){

            if (unsigned(techAutBytes) == 1){
                autentication(msg);
            }

            else if (unsigned(techAutBytes) == 2){
                autentication2(msg);
            }
            else if (unsigned(techAutBytes) == 4){
                autentication4(msg);
            }
            else if (unsigned(techAutBytes) == 8){
                autentication8(msg);
            }
        }else {
            
        

        if (tech.compare("OnOff") == 0){
            insertDelayOnOff(msg);
        }else if (tech.compare("LBitsNPackets") == 0){
            if (unsigned(techBytes) == 2)
                insertDelayList(msg);
            else if (unsigned(techBytes) == 4)
                insertDelayListExp(msg);
            else if (unsigned(techBytes) == 8)
                insertDelayListExp8(msg);
        }else if (tech.compare("TimeReplay") == 0){
            if (unsigned(techBytes) == 2)
                insertDelay2Lists(msg);
            else if (unsigned(techBytes) == 4)
                insertDelay2ListsExp(msg);
            else if (unsigned(techBytes) == 8)
                insertDelay2ListsExp8(msg);
        }else
            DSME_SIM_ASSERT(false);
        }
        }

    std::bitset<8> CovertHelper::getBitsetFromChar(char s){
        return std::bitset<8>(s);
    }

    void CovertHelper::insertDelayOnOff(IDSMEMessage* msg) {
        LOG_DEBUG("LALALALALALALALALAALLALALALALALALALALALALALALALALALALALALALALALALALALALAOO");
        std::ofstream myfile;
        std::ifstream file("tmp.csv");

        if(unsigned(this->dsme.getPlatform().getSrcAddress()) == 2 && unsigned(msg->getHeader().getDestAddr().getShortAddress()) == 1){
        
                while (file >> allPackets){
                    allPackets+= ((msg->getTotalSymbols() - 120U) / 2);
                }

                myfile.open ("tmp.csv");
                myfile << std::to_string(allPackets);
                myfile.close(); 

                //lastMessage->setPayloadData(48);

                LOG_DEBUG("ADDRESS: " << unsigned(this->dsme.getPlatform().getSrcAddress()));
                //LOG_DEBUG("MESSAGE TO SEND: " << messageInBits);
                LOG_DEBUG("LETTER TO SEND: " << messageToEncode[letterCounter]);
                LOG_DEBUG("LETTERCOUNTER: " << letterCounter);
                //LOG_DEBUG("BIT TO SENDDDDD: " << messageInBits.to_string()[bitCounter]);
                LOG_DEBUG("SIZEEEEEE: " << msg->getTotalSymbols());

                uint8_t so = unsigned(this->dsme.getMAC_PIB().macSuperframeOrder);
                uint32_t symbols = 1;//const_redefines::macLIFSPeriod * 3;

                double millis = (double) omnetpp::simTime().dbl();

                if(!first && ((millis - lastTime)*1000 > 100)){
                    if(letterCounter == 0)
                        letterCounter = 208;
                    letterCounter--;
                    covertPacketsCounter--;
                    bitssent--;
                }
                auto lalala = std::find(diffChars.begin(), diffChars.end(), messageToEncode[letterCounter]);
                int x = lalala - diffChars.begin();
                if(first){
                    lastTime = millis;
                    first = false;
                    covertPackets = 0;
                    covertPacketsCounter = 0;
                    this->dsme.getPlatform().startDelay(NOW + unsigned(symbols));
                    return;
                }else{
                    symbols = unsigned(diffIntervals[x]);
                    letterCounter++;
                    if(letterCounter >= 208)
                        letterCounter = 0;
                    lastTime = millis;
                    covertPackets++;
                    covertPacketsCounter++;
                    bitssent++;
                    myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                    myfile << (std::to_string(millis) + "," + std::to_string(covertPacketsCounter) +  "," + std::to_string(allPackets) + "," + std::to_string(bitssent/8) + ",");
                    myfile.close();
                    this->dsme.getPlatform().startDelay(NOW + unsigned(symbols));
                    return;
                }
        }
        this->dsme.getMessageDispatcher().sendDoneGTS();

    }

    void CovertHelper::insertDelayList(IDSMEMessage* msg) {
        LOG_DEBUG("LALALALALALALALALAALLALALALALALALALALALALALALALALALALALALALALALALALALALALBNP2");
        std::ofstream myfile;
        //lastMessage = msg;
        //lastResponse = response;
        //messageToEncode = "abcabcabcabcabc";
        std::string diffChars2[] {"00", "01", "10", "11"};
        int size = sizeof(diffChars2)/sizeof(diffChars2[0]);

        /*diffChars.clear();
        diffChars.push_back("00");
        diffChars.push_back("01");
        diffChars.push_back("10");
        diffChars.push_back("11");*/

        diffIntervals.clear();
        diffIntervals.push_back(2);
        diffIntervals.push_back(3);
        diffIntervals.push_back(4);
        diffIntervals.push_back(5);

        std::ifstream file("tmp.csv");

        // 1 symbol --> 4 bits
        // 2 symbols --> 1 byte 

        ///////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////TESTE//////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////

        if(unsigned(this->dsme.getPlatform().getSrcAddress()) == 2 && unsigned(msg->getHeader().getDestAddr().getShortAddress()) == 1){
        
                while (file >> allPackets){
                    allPackets+= ((msg->getTotalSymbols() - 120U) / 2);
                }

                myfile.open ("tmp.csv");
                myfile << std::to_string(allPackets);
                myfile.close(); 

                //lastMessage->setPayloadData(48);

                LOG_DEBUG("ADDRESS: " << unsigned(this->dsme.getPlatform().getSrcAddress()));
                //LOG_DEBUG("MESSAGE TO SEND: " << messageInBits);
                LOG_DEBUG("LETTER TO SEND: " << messageToEncode[letterCounter] << messageToEncode[letterCounter+1]);
                LOG_DEBUG("COUNTER: " << bitCounter);
                //LOG_DEBUG("BIT TO SENDDDDD: " << messageInBits.to_string()[bitCounter]);
                LOG_DEBUG("SIZEEEEEE: " << msg->getTotalSymbols());

                uint8_t so = unsigned(this->dsme.getMAC_PIB().macSuperframeOrder);
                // base superframe duration = 960
                // base slot duration = 60
                // 1.5% da slot --> delay singular
                LOG_DEBUG("LIFSSSSSSS: " << unsigned(const_redefines::macLIFSPeriod));
                uint32_t symbols = 1;//const_redefines::macLIFSPeriod;//60U * (pow(2, unsigned(so))) * 0.015;

                double millis = (double) omnetpp::simTime().dbl();

                LOG_DEBUG("INTERVALLLLLLLLLLLLLL: " << (millis - lastTime)*1000);
                if(!first && (((millis - lastTime)*1000 > 100))){
                    /*if(bitCounter == 0){
                        letterCounter--;
                        //messageInBits = getBitsetFromChar(messageToEncode[letterCounter]);
                        bitCounter = 7;
                        bitssent--;
                    }else{
                        bitCounter--;
                    }*/
                    if(letterCounter == 0)
                        letterCounter = 208;
                    letterCounter-=2;
                    covertPacketsCounter--;
                    bitssent-=2;
                }


                //auto lalala = std::find(diffChars2, sizeof(diffChars2)/sizeof(diffChars2[0]), messageToEncode[letterCounter] + messageToEncode[letterCounter+1]);
                //int x = std::distance(diffChars2, lalala);

                LOG_DEBUG("LETTERCOUNTER: " << letterCounter);
                if(first){
                    //ifsDuration = (uint16_t) (700U);
                    lastTime = millis;
                    first = false;
                    covertPackets = 0;
                    covertPacketsCounter = 0;
                    this->dsme.getPlatform().startDelay(NOW + unsigned(symbols));
                    return;
                }else{
                    int i = 0;
                    while (i < size)
                    {
                        if (diffChars2[i].compare(std::string(1,messageToEncode.at(letterCounter)) + messageToEncode.at(letterCounter+1)) == 0) {
                            break;
                        }
                        i++;
                    }
                    LOG_DEBUG("ENTROU");
                    symbols = /*const_redefines::macLIFSPeriod **/ unsigned(2 + static_cast<unsigned char>(diffIntervals[i]*ALPHA));//60U * (pow(2, unsigned(so))) * 0.015;
                    LOG_DEBUG("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
                    LOG_DEBUG("INDEX = " << i);
                    LOG_DEBUG("INTERVAL = " << unsigned(diffIntervals[i]));
                    std::cout << "COISE-E-TAL   " << letterCounter << "   " << messageToEncode[letterCounter] << "   " << i << "   " << unsigned(diffIntervals[i]) << std::endl;
                    letterCounter+=2;
                    if(letterCounter >= 208)
                        letterCounter = 0;
                    lastTime = millis;
                    covertPackets++;
                    covertPacketsCounter++;
                    bitssent+=2;
                    myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                    myfile << (std::to_string(millis) + "," + std::to_string(covertPacketsCounter) +  "," + std::to_string(allPackets) + "," + std::to_string(bitssent) + ",");
                    myfile.close(); 
                    //ifsDuration = (uint16_t) (300U);
                    //this->dsme.getEventDispatcher().setupIFSTimer(false);
                    //std::this_thread::sleep_for(std::chrono::milliseconds(100));
                    this->dsme.getPlatform().startDelay(NOW + unsigned(symbols));
                    return;
                }
        }
        this->dsme.getMessageDispatcher().sendDoneGTS();
    }



    void CovertHelper::insertDelayListExp(IDSMEMessage* msg) {
        LOG_DEBUG("LALALALALALALALALAALLALALALALALALALALALALALALALALALALALALALALALALALALALALBNP4");
        std::ofstream myfile;
        //lastMessage = msg;
        //lastResponse = response;
        //messageToEncode = "abcabcabcabcabc";

        uint32_t diffIntervals[BYTE_NUM];
        std::vector<std::string> possibleCombinations {
            "0000",
            "0001",
            "0010",
            "0011",
            "0100",
            "0101",
            "0110",
            "0111",
            "1000",
            "1001",
            "1010",
            "1011",
            "1100",
            "1101",
            "1110",
            "1111",
        };

        std::ifstream file("tmp.csv");

        // 1 symbol --> 4 bits
        // 2 symbols --> 1 byte 

        ///////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////TESTE//////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////

        if(unsigned(this->dsme.getPlatform().getSrcAddress()) == 2 && unsigned(msg->getHeader().getDestAddr().getShortAddress()) == 1){

                uint16_t counter = 2;

                for(int i=0; i<BYTE_NUM; i++){
                    diffIntervals[i] = counter;
                    counter+=ALPHA;
                }
        
                while (file >> allPackets){
                    allPackets+= ((msg->getTotalSymbols() - 120U) / 2);
                }

                myfile.open ("tmp.csv");
                myfile << std::to_string(allPackets);
                myfile.close(); 

                //lastMessage->setPayloadData(48);

                LOG_DEBUG("ADDRESS: " << unsigned(this->dsme.getPlatform().getSrcAddress()));
                //LOG_DEBUG("MESSAGE TO SEND: " << messageInBits);
                LOG_DEBUG("LETTER TO SEND: " << messageToEncode[letterCounter] << messageToEncode[letterCounter+1]);
                LOG_DEBUG("COUNTER: " << bitCounter);
                //LOG_DEBUG("BIT TO SENDDDDD: " << messageInBits.to_string()[bitCounter]);
                LOG_DEBUG("SIZEEEEEE: " << msg->getTotalSymbols());

                uint8_t so = unsigned(this->dsme.getMAC_PIB().macSuperframeOrder);
                // base superframe duration = 960
                // base slot duration = 60
                // 1.5% da slot --> delay singular
                LOG_DEBUG("LIFSSSSSSS: " << unsigned(const_redefines::macLIFSPeriod));
                uint32_t symbols = 1;//const_redefines::macLIFSPeriod;//60U * (pow(2, unsigned(so))) * 0.015;

                double millis = (double) omnetpp::simTime().dbl();

                LOG_DEBUG("INTERVALLLLLLLLLLLLLL: " << (millis - lastTime)*1000);
                if(!first && (((millis - lastTime)*1000 > 100))){
                    /*if(bitCounter == 0){
                        letterCounter--;
                        //messageInBits = getBitsetFromChar(messageToEncode[letterCounter]);
                        bitCounter = 7;
                        bitssent--;
                    }else{
                        bitCounter--;
                    }*/
                    if(letterCounter == 0)
                        letterCounter = 208;
                    letterCounter-=4;
                    covertPacketsCounter--;
                    bitssent-=4;
                }


                //auto lalala = std::find(diffChars2, sizeof(diffChars2)/sizeof(diffChars2[0]), messageToEncode[letterCounter] + messageToEncode[letterCounter+1]);
                //int x = std::distance(diffChars2, lalala);

                LOG_DEBUG("LETTERCOUNTER: " << letterCounter);
                if(first){
                    //ifsDuration = (uint16_t) (700U);
                    lastTime = millis;
                    first = false;
                    covertPackets = 0;
                    covertPacketsCounter = 0;
                    this->dsme.getPlatform().startDelay(NOW + unsigned(symbols));
                    return;
                }else{
                    int i = 0;
                    while (i < BYTE_NUM)
                    {

                        if((std::string(1,messageToEncode.at(letterCounter))
                                        + messageToEncode.at(letterCounter+1)
                                        + messageToEncode.at(letterCounter+2)
                                        + messageToEncode.at(letterCounter+3)).compare(possibleCombinations.at(i)) == 0)
                            break;
                        i++;
                    }
                    LOG_DEBUG("ENTROU");
                    symbols = /*const_redefines::macLIFSPeriod **/ unsigned(diffIntervals[i]);//60U * (pow(2, unsigned(so))) * 0.015;
                    LOG_DEBUG("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
                    LOG_DEBUG("INDEX = " << i);
                    for(int q=0; q<(sizeof(diffIntervals)/sizeof(*diffIntervals)); q++)
                        LOG_DEBUG("COUNTERRRRRRRRR: " << unsigned(diffIntervals[q]));
                    LOG_DEBUG("INTERVAL = " << unsigned(diffIntervals[i]));
                    std::cout << "COISE-E-TAL   " << letterCounter << "   " << messageToEncode[letterCounter] << "   " << i << "   " << unsigned(diffIntervals[i]) << std::endl;
                    letterCounter+=4;
                    if(letterCounter >= 208)
                        letterCounter = 0;
                    lastTime = millis;
                    covertPackets++;
                    covertPacketsCounter++;
                    bitssent+=4;
                    myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                    myfile << (std::to_string(millis) + "," + std::to_string(covertPacketsCounter) +  "," + std::to_string(allPackets) + "," + std::to_string(bitssent) + ",");
                    myfile.close(); 
                    //ifsDuration = (uint16_t) (300U);
                    //this->dsme.getEventDispatcher().setupIFSTimer(false);
                    //std::this_thread::sleep_for(std::chrono::milliseconds(100));
                    this->dsme.getPlatform().startDelay(NOW + unsigned(symbols));
                    return;
                }
        }
        this->dsme.getMessageDispatcher().sendDoneGTS();
    }



    void CovertHelper::insertDelayListExp8(IDSMEMessage* msg) {
        LOG_DEBUG("LALALALALALALALALAALLALALALALALALALALALALALALALALALALALALALALALALALALALALBNP8");
        std::ofstream myfile;
        //lastMessage = msg;
        //lastResponse = response;
        //messageToEncode = "abcabcabcabcabc";

        uint32_t diffIntervals[BYTE_NUM_8];

        std::ifstream file("tmp.csv");

        // 1 symbol --> 4 bits
        // 2 symbols --> 1 byte 

        ///////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////TESTE//////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////

        if(unsigned(this->dsme.getPlatform().getSrcAddress()) == 2 && unsigned(msg->getHeader().getDestAddr().getShortAddress()) == 1){

                uint16_t counter = 2;

                for(int i=0; i<BYTE_NUM_8; i++){
                    diffIntervals[i] = counter;
                    counter+=ALPHA;
                }
        
                while (file >> allPackets){
                    allPackets+= ((msg->getTotalSymbols() - 120U) / 2);
                }

                myfile.open ("tmp.csv");
                myfile << std::to_string(allPackets);
                myfile.close(); 

                //lastMessage->setPayloadData(48);

                LOG_DEBUG("ADDRESS: " << unsigned(this->dsme.getPlatform().getSrcAddress()));
                //LOG_DEBUG("MESSAGE TO SEND: " << messageInBits);
                LOG_DEBUG("LETTER TO SEND: " << messageToEncode[letterCounter] << messageToEncode[letterCounter+1]);
                LOG_DEBUG("COUNTER: " << bitCounter);
                //LOG_DEBUG("BIT TO SENDDDDD: " << messageInBits.to_string()[bitCounter]);
                LOG_DEBUG("SIZEEEEEE: " << msg->getTotalSymbols());

                uint8_t so = unsigned(this->dsme.getMAC_PIB().macSuperframeOrder);
                // base superframe duration = 960
                // base slot duration = 60
                // 1.5% da slot --> delay singular
                LOG_DEBUG("LIFSSSSSSS: " << unsigned(const_redefines::macLIFSPeriod));
                uint32_t symbols = 1;//const_redefines::macLIFSPeriod;//60U * (pow(2, unsigned(so))) * 0.015;

                double millis = (double) omnetpp::simTime().dbl();

                LOG_DEBUG("INTERVALLLLLLLLLLLLLL: " << (millis - lastTime)*1000);
                if(!first && (((millis - lastTime)*1000 > 100))){
                    /*if(bitCounter == 0){
                        letterCounter--;
                        //messageInBits = getBitsetFromChar(messageToEncode[letterCounter]);
                        bitCounter = 7;
                        bitssent--;
                    }else{
                        bitCounter--;
                    }*/
                    if(letterCounter == 0)
                        letterCounter = 208;
                    letterCounter-=8;
                    covertPacketsCounter--;
                    bitssent-=8;
                }


                //auto lalala = std::find(diffChars2, sizeof(diffChars2)/sizeof(diffChars2[0]), messageToEncode[letterCounter] + messageToEncode[letterCounter+1]);
                //int x = std::distance(diffChars2, lalala);

                LOG_DEBUG("LETTERCOUNTER: " << letterCounter);
                if(first){
                    //ifsDuration = (uint16_t) (700U);
                    lastTime = millis;
                    first = false;
                    covertPackets = 0;
                    covertPacketsCounter = 0;
                    this->dsme.getPlatform().startDelay(NOW + unsigned(symbols));
                    return;
                }else{
                    int i = 0;
                    while (i < BYTE_NUM_8)
                    {
                        if((std::string(1,messageToEncode.at(letterCounter))
                                        + messageToEncode.at(letterCounter+1)
                                        + messageToEncode.at(letterCounter+2)
                                        + messageToEncode.at(letterCounter+3)
                                        + messageToEncode.at(letterCounter+4)
                                        + messageToEncode.at(letterCounter+5)
                                        + messageToEncode.at(letterCounter+6)
                                        + messageToEncode.at(letterCounter+7)).compare(possibleCombinations8.at(i)) == 0)
                            break;
                        i++;
                    }
                    LOG_DEBUG("ENTROU");
                    symbols = /*const_redefines::macLIFSPeriod **/ unsigned(diffIntervals[i]);//60U * (pow(2, unsigned(so))) * 0.015;
                    LOG_DEBUG("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
                    LOG_DEBUG("INDEX = " << i);
                    LOG_DEBUG("LETTER COUNTER = " << letterCounter);
                    LOG_DEBUG("INTERVAL = " << unsigned(diffIntervals[i]));
                    std::cout << "COISE-E-TAL   " << letterCounter << "   " << messageToEncode[letterCounter] << "   " << i << "   " << unsigned(diffIntervals[i]) << std::endl;
                    letterCounter+=8;
                    if(letterCounter >= 208)
                        letterCounter = 0;
                    lastTime = millis;
                    covertPackets++;
                    covertPacketsCounter++;
                    bitssent+=8;
                    myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                    myfile << (std::to_string(millis) + "," + std::to_string(covertPacketsCounter) +  "," + std::to_string(allPackets) + "," + std::to_string(bitssent) + ",");
                    myfile.close(); 
                    //ifsDuration = (uint16_t) (300U);
                    //this->dsme.getEventDispatcher().setupIFSTimer(false);
                    //std::this_thread::sleep_for(std::chrono::milliseconds(100));
                    this->dsme.getPlatform().startDelay(NOW + unsigned(symbols));
                    return;
                }
        }
        this->dsme.getMessageDispatcher().sendDoneGTS();
    }

    void CovertHelper::insertDelay2Lists(IDSMEMessage* msg) {
        LOG_DEBUG("LALALALALALALALALAALLALALALALALALALALALALALALALALALALALALALALALALALALALATR2");
        std::cout << "TR2" << std::endl;
        std::ofstream myfile;
        //lastMessage = msg;
        //lastResponse = response;

        std::vector<uint8_t> diffIntervals00 {2,6,10};
        std::vector<uint8_t> diffIntervals01 {3,7,11};
        std::vector<uint8_t> diffIntervals10 {4,8,12};
        std::vector<uint8_t> diffIntervals11 {5,9,13};

        std::ifstream file("tmp.csv");

        // 1 symbol --> 4 bits
        // 2 symbols --> 1 byte 

        ///////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////TESTE//////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////

        if(unsigned(this->dsme.getPlatform().getSrcAddress()) == 2 && unsigned(msg->getHeader().getDestAddr().getShortAddress()) == 1){
        
                while (file >> allPackets){
                    allPackets+= ((msg->getTotalSymbols() - 120U) / 2);
                }

                myfile.open ("tmp.csv");
                myfile << std::to_string(allPackets);
                myfile.close(); 

                //lastMessage->setPayloadData(48);

                LOG_DEBUG("ADDRESS: " << unsigned(this->dsme.getPlatform().getSrcAddress()));
                //LOG_DEBUG("MESSAGE TO SEND: " << messageInBits);
                
                //LOG_DEBUG("BIT TO SENDDDDD: " << messageInBits.to_string()[bitCounter]);
                LOG_DEBUG("SIZEEEEEE: " << msg->getTotalSymbols());

                uint8_t so = unsigned(this->dsme.getMAC_PIB().macSuperframeOrder);
                // base superframe duration = 960
                // base slot duration = 60
                // 1.5% da slot --> delay singular
            
                uint32_t symbols = 1;//const_redefines::macLIFSPeriod;//60U * (pow(2, unsigned(so))) * 0.015;

                double millis = (double) omnetpp::simTime().dbl();

                LOG_DEBUG("INTERVALLLLLLLLLLLLLL: " << (millis - lastTime)*1000);
                if(!first && ((millis - lastTime)*1000 > 100)){
                    /*if(bitCounter == 0){
                        letterCounter--;
                        //messageInBits = getBitsetFromChar(messageToEncode[letterCounter]);
                        bitCounter = 7;
                        bitssent--;
                    }else{
                        bitCounter--;
                    }*/
                    if(letterCounter == 0)
                        letterCounter = 208;
                    letterCounter-=2;
                    covertPacketsCounter--;
                    bitssent-=2;
                }
                //auto lalala = std::find(diffChars.begin(), diffChars.end(), messageToEncode[letterCounter]);
                //int x = lalala - diffChars.begin();
                if(first){
                    //ifsDuration = (uint16_t) (700U);
                    std::cout << "FIRST - " << unsigned(symbols) << std::endl;
                    lastTime = millis;
                    first = false;
                    covertPackets = 0;
                    covertPacketsCounter = 0;
                    this->dsme.getPlatform().startDelay(NOW + unsigned(symbols));
                    return;
                }else{
                    int x;
                    std::random_device dev;
                    std::mt19937 rng(dev());
                    std::uniform_int_distribution<std::mt19937::result_type> dist6(0,diffIntervals00.size()-1);
                    auto lala = dist6(rng);
                    if((std::string(1,messageToEncode.at(letterCounter)) + messageToEncode.at(letterCounter+1)).compare("00") == 0)
                        x = diffIntervals00[lala];
                    else if((std::string(1,messageToEncode.at(letterCounter)) + messageToEncode.at(letterCounter+1)).compare("01") == 0)
                        x = diffIntervals01[lala];
                    else if((std::string(1,messageToEncode.at(letterCounter)) + messageToEncode.at(letterCounter+1)).compare("10") == 0)
                        x = diffIntervals10[lala];
                    else if((std::string(1,messageToEncode.at(letterCounter)) + messageToEncode.at(letterCounter+1)).compare("11") == 0)
                        x = diffIntervals11[lala];
                    else 
                        DSME_SIM_ASSERT(false);

                    /*if(messageToEncode[letterCounter] == '0'){
                        auto lala = dist6(rng);
                        x = diffIntervals0[lala];
                        std::cout << "LALALALALA - " << lala << std::endl;
                    }else{
                        auto lala = dist6(rng);
                        x = diffIntervals1[lala];
                        std::cout << "LALALALALA - " << lala << std::endl;
                    }*/
                    std::cout << "INTTTTTTT - " << x << std::endl;
                    LOG_DEBUG("ENTROU");
                    symbols = /*const_redefines::macLIFSPeriod */ x;//60U * (pow(2, unsigned(so))) * 0.015;
                    LOG_DEBUG("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
                    LOG_DEBUG("INDEX = " << x);
                    letterCounter+=2;
                    if(letterCounter >= 208)
                        letterCounter = 0;
                    lastTime = millis;
                    covertPackets++;
                    covertPacketsCounter++;
                    bitssent+=2;
                    myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                    myfile << (std::to_string(millis) + "," + std::to_string(covertPacketsCounter) +  "," + std::to_string(allPackets) + "," + std::to_string(bitssent/8) + ",");
                    myfile.close(); 
                    //ifsDuration = (uint16_t) (300U);
                    //this->dsme.getEventDispatcher().setupIFSTimer(false);
                    //std::this_thread::sleep_for(std::chrono::milliseconds(100));
                    this->dsme.getPlatform().startDelay(NOW + unsigned(symbols*ALPHA));
                    return;
                }
        }
        this->dsme.getMessageDispatcher().sendDoneGTS();
    }

    void CovertHelper::insertDelay2ListsExp(IDSMEMessage* msg) {
        LOG_DEBUG("LALALALALALALALALAALLALALALALALALALALALALALALALALALALALALALALALALALALALATR4");
        std::ofstream myfile;
        //lastMessage = msg;
        //lastResponse = response;

        uint32_t diffIntervals[BYTE_NUM][LIST_SIZE];
        std::vector<std::string> possibleCombinations {
            "0000",
            "0001",
            "0010",
            "0011",
            "0100",
            "0101",
            "0110",
            "0111",
            "1000",
            "1001",
            "1010",
            "1011",
            "1100",
            "1101",
            "1110",
            "1111",
        };

        std::ifstream file("tmp.csv");

        // 1 symbol --> 4 bits
        // 2 symbols --> 1 byte 

        ///////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////TESTE//////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////

        if(unsigned(this->dsme.getPlatform().getSrcAddress()) == 2 && unsigned(msg->getHeader().getDestAddr().getShortAddress()) == 1){

                int counter = 2;

                for(int i=0; i<LIST_SIZE; i++){
                    for(int j=0; j<BYTE_NUM; j++){
                        diffIntervals[j][i] = counter;
                        counter+=ALPHA;
                    }
                    
                }
        
                while (file >> allPackets){
                    allPackets+= ((msg->getTotalSymbols() - 120U) / 2);
                }

                myfile.open ("tmp.csv");
                myfile << std::to_string(allPackets);
                myfile.close(); 

                //lastMessage->setPayloadData(48);

                LOG_DEBUG("ADDRESS: " << unsigned(this->dsme.getPlatform().getSrcAddress()));
                //LOG_DEBUG("MESSAGE TO SEND: " << messageInBits);
                LOG_DEBUG("LETTER TO SEND: " << messageToEncode[letterCounter]);
                LOG_DEBUG("COUNTER: " << bitCounter);
                //LOG_DEBUG("BIT TO SENDDDDD: " << messageInBits.to_string()[bitCounter]);
                LOG_DEBUG("SIZEEEEEE: " << msg->getTotalSymbols());

                uint8_t so = unsigned(this->dsme.getMAC_PIB().macSuperframeOrder);
                // base superframe duration = 960
                // base slot duration = 60
                // 1.5% da slot --> delay singular
                LOG_DEBUG("LIFSSSSSSS: " << unsigned(const_redefines::macLIFSPeriod));
                uint32_t symbols = 1;//const_redefines::macLIFSPeriod;//60U * (pow(2, unsigned(so))) * 0.015;

                double millis = (double) omnetpp::simTime().dbl();

                LOG_DEBUG("INTERVALLLLLLLLLLLLLL: " << (millis - lastTime)*1000);
                if(!first && ((millis - lastTime)*1000 > 100)){
                    /*if(bitCounter == 0){
                        letterCounter--;
                        //messageInBits = getBitsetFromChar(messageToEncode[letterCounter]);
                        bitCounter = 7;
                        bitssent--;
                    }else{
                        bitCounter--;
                    }*/
                    if(letterCounter == 0)
                        letterCounter = 208;
                    letterCounter-=4;
                    covertPacketsCounter--;
                    bitssent-=4;
                }
                //auto lalala = std::find(diffChars.begin(), diffChars.end(), messageToEncode[letterCounter]);
                //int x = lalala - diffChars.begin();
                if(first){
                    //ifsDuration = (uint16_t) (700U);
                    std::cout << "FIRST - " << unsigned(symbols) << std::endl;
                    lastTime = millis;
                    first = false;
                    covertPackets = 0;
                    covertPacketsCounter = 0;
                    this->dsme.getPlatform().startDelay(NOW + unsigned(symbols));
                    return;
                }else{
                    int x;
                    std::random_device dev;
                    std::mt19937 rng(dev());
                    std::uniform_int_distribution<std::mt19937::result_type> dist6(0,LIST_SIZE-1);
                    auto lala = dist6(rng);
                    for(int i=0; i<possibleCombinations.size(); i++)
                        if((std::string(1,messageToEncode.at(letterCounter))
                                        + messageToEncode.at(letterCounter+1)
                                        + messageToEncode.at(letterCounter+2)
                                        + messageToEncode.at(letterCounter+3)).compare(possibleCombinations.at(i)) == 0)
                            x = diffIntervals[i][lala];

                    /*if((std::string(1,messageToEncode.at(letterCounter)) + messageToEncode.at(letterCounter+1)).compare("00") == 0)
                        x = diffIntervals00[lala];
                    else if((std::string(1,messageToEncode.at(letterCounter)) + messageToEncode.at(letterCounter+1)).compare("01") == 0)
                        x = diffIntervals01[lala];
                    else if((std::string(1,messageToEncode.at(letterCounter)) + messageToEncode.at(letterCounter+1)).compare("10") == 0)
                        x = diffIntervals10[lala];
                    else if((std::string(1,messageToEncode.at(letterCounter)) + messageToEncode.at(letterCounter+1)).compare("11") == 0)
                        x = diffIntervals11[lala];
                    else 
                        DSME_SIM_ASSERT(false);

                    if(messageToEncode[letterCounter] == '0'){
                        auto lala = dist6(rng);
                        x = diffIntervals0[lala];
                        std::cout << "LALALALALA - " << lala << std::endl;
                    }else{
                        auto lala = dist6(rng);
                        x = diffIntervals1[lala];
                        std::cout << "LALALALALA - " << lala << std::endl;
                    }*/
                    std::cout << "INTTTTTTT - " << x << std::endl;
                    LOG_DEBUG("ENTROU");
                    symbols = /*const_redefines::macLIFSPeriod */ x;//60U * (pow(2, unsigned(so))) * 0.015;
                    LOG_DEBUG("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
                    LOG_DEBUG("INDEX = " << x);
                    letterCounter+=4;
                    if(letterCounter >= 208)
                        letterCounter = 0;
                    lastTime = millis;
                    covertPackets++;
                    covertPacketsCounter++;
                    bitssent+=4;
                    myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                    myfile << (std::to_string(millis) + "," + std::to_string(covertPacketsCounter) +  "," + std::to_string(allPackets) + "," + std::to_string(bitssent/8) + ",");
                    myfile.close(); 
                    //ifsDuration = (uint16_t) (300U);
                    //this->dsme.getEventDispatcher().setupIFSTimer(false);
                    //std::this_thread::sleep_for(std::chrono::milliseconds(100));
                    this->dsme.getPlatform().startDelay(NOW + unsigned(symbols));
                    return;
                }
        }
        this->dsme.getMessageDispatcher().sendDoneGTS();
    }

    void CovertHelper::insertDelay2ListsExp8(IDSMEMessage* msg) {
        LOG_DEBUG("LALALALALALALALALAALLALALALALALALALALALALALALALALALALALALALALALALALALALATR8");
        std::ofstream myfile;
        //lastMessage = msg;
        //lastResponse = response;

        uint32_t diffIntervals[BYTE_NUM_8][LIST_SIZE];

        std::ifstream file("tmp.csv");

        // 1 symbol --> 4 bits
        // 2 symbols --> 1 byte 

        ///////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////TESTE//////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////

        if(unsigned(this->dsme.getPlatform().getSrcAddress()) == 2 && unsigned(msg->getHeader().getDestAddr().getShortAddress()) == 1){

                int counter = 2;

                for(int i=0; i<LIST_SIZE; i++){
                    for(int j=0; j<BYTE_NUM_8; j++){
                        diffIntervals[j][i] = counter;
                        counter+=ALPHA;
                    }
                    
                }
        
                while (file >> allPackets){
                    allPackets+= ((msg->getTotalSymbols() - 120U) / 2);
                }

                myfile.open ("tmp.csv");
                myfile << std::to_string(allPackets);
                myfile.close(); 

                //lastMessage->setPayloadData(48);

                LOG_DEBUG("ADDRESS: " << unsigned(this->dsme.getPlatform().getSrcAddress()));
                //LOG_DEBUG("MESSAGE TO SEND: " << messageInBits);
                LOG_DEBUG("LETTER TO SEND: " << messageToEncode[letterCounter]);
                LOG_DEBUG("COUNTER: " << bitCounter);
                //LOG_DEBUG("BIT TO SENDDDDD: " << messageInBits.to_string()[bitCounter]);
                LOG_DEBUG("SIZEEEEEE: " << msg->getTotalSymbols());

                uint8_t so = unsigned(this->dsme.getMAC_PIB().macSuperframeOrder);
                // base superframe duration = 960
                // base slot duration = 60
                // 1.5% da slot --> delay singular
                LOG_DEBUG("LIFSSSSSSS: " << unsigned(const_redefines::macLIFSPeriod));
                uint32_t symbols = 1;//const_redefines::macLIFSPeriod;//60U * (pow(2, unsigned(so))) * 0.015;

                double millis = (double) omnetpp::simTime().dbl();

                LOG_DEBUG("INTERVALLLLLLLLLLLLLL: " << (millis - lastTime)*1000);
                if(!first && ((millis - lastTime)*1000 > 100)){
                    /*if(bitCounter == 0){
                        letterCounter--;
                        //messageInBits = getBitsetFromChar(messageToEncode[letterCounter]);
                        bitCounter = 7;
                        bitssent--;
                    }else{
                        bitCounter--;
                    }*/
                    if(letterCounter == 0)
                        letterCounter = 208;
                    letterCounter-=8;
                    covertPacketsCounter--;
                    bitssent-=8;
                }
                //auto lalala = std::find(diffChars.begin(), diffChars.end(), messageToEncode[letterCounter]);
                //int x = lalala - diffChars.begin();
                if(first){
                    //ifsDuration = (uint16_t) (700U);
                    std::cout << "FIRST - " << unsigned(symbols) << std::endl;
                    lastTime = millis;
                    first = false;
                    covertPackets = 0;
                    covertPacketsCounter = 0;
                    this->dsme.getPlatform().startDelay(NOW + unsigned(symbols));
                    return;
                }else{
                    int x;
                    std::random_device dev;
                    std::mt19937 rng(dev());
                    std::uniform_int_distribution<std::mt19937::result_type> dist6(0,LIST_SIZE-1);
                    auto lala = dist6(rng);
                    for(int i=0; i<BYTE_NUM_8; i++)
                        if((std::string(1,messageToEncode.at(letterCounter))
                                        + messageToEncode.at(letterCounter+1)
                                        + messageToEncode.at(letterCounter+2)
                                        + messageToEncode.at(letterCounter+3)
                                        + messageToEncode.at(letterCounter+4)
                                        + messageToEncode.at(letterCounter+5)
                                        + messageToEncode.at(letterCounter+6)
                                        + messageToEncode.at(letterCounter+7)).compare(possibleCombinations8.at(i)) == 0)
                            x = diffIntervals[i][lala];

                    /*if((std::string(1,messageToEncode.at(letterCounter)) + messageToEncode.at(letterCounter+1)).compare("00") == 0)
                        x = diffIntervals00[lala];
                    else if((std::string(1,messageToEncode.at(letterCounter)) + messageToEncode.at(letterCounter+1)).compare("01") == 0)
                        x = diffIntervals01[lala];
                    else if((std::string(1,messageToEncode.at(letterCounter)) + messageToEncode.at(letterCounter+1)).compare("10") == 0)
                        x = diffIntervals10[lala];
                    else if((std::string(1,messageToEncode.at(letterCounter)) + messageToEncode.at(letterCounter+1)).compare("11") == 0)
                        x = diffIntervals11[lala];
                    else 
                        DSME_SIM_ASSERT(false);

                    if(messageToEncode[letterCounter] == '0'){
                        auto lala = dist6(rng);
                        x = diffIntervals0[lala];
                        std::cout << "LALALALALA - " << lala << std::endl;
                    }else{
                        auto lala = dist6(rng);
                        x = diffIntervals1[lala];
                        std::cout << "LALALALALA - " << lala << std::endl;
                    }*/
                    std::cout << "INTTTTTTT - " << x << std::endl;
                    LOG_DEBUG("ENTROU");
                    symbols = /*const_redefines::macLIFSPeriod */ x;//60U * (pow(2, unsigned(so))) * 0.015;
                    LOG_DEBUG("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
                    LOG_DEBUG("INDEX = " << x);
                    letterCounter+=8;
                    if(letterCounter >= 208)
                        letterCounter = 0;
                    lastTime = millis;
                    covertPackets++;
                    covertPacketsCounter++;
                    bitssent+=8;
                    myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                    myfile << (std::to_string(millis) + "," + std::to_string(covertPacketsCounter) +  "," + std::to_string(allPackets) + "," + std::to_string(bitssent/8) + ",");
                    myfile.close(); 
                    //ifsDuration = (uint16_t) (300U);
                    //this->dsme.getEventDispatcher().setupIFSTimer(false);
                    //std::this_thread::sleep_for(std::chrono::milliseconds(100));
                    this->dsme.getPlatform().startDelay(NOW + unsigned(symbols));
                    return;
                }
        }
        this->dsme.getMessageDispatcher().sendDoneGTS();
    }

    // The main recursive method
    // to print all possible
    // strings of length k
    void CovertHelper::printAllKLengthRec(char set[], std::string prefix,
                                        int n, int k)
    {
        
        // Base case: k is 0,
        // print prefix
        if (k == 0)
        {
            possibleCombinations8.push_back(prefix);
            return;
        }
    
        // One by one add all characters
        // from set and recursively
        // call for k equals to k-1
        for (int i = 0; i < n; i++)
        {
            std::string newPrefix;
            
            // Next character of input added
            newPrefix = prefix + set[i];
            
            // k is decreased, because
            // we have added a new character
            printAllKLengthRec(set, newPrefix, n, k - 1);
        }
    
    }
    
    void CovertHelper::printAllKLength(char set[], int k, int n)
    {
        printAllKLengthRec(set, "", n, k);
    }

    IDSMEMessage* CovertHelper::modifyPayload(IDSMEMessage* msg){
        if(unsigned(this->dsme.getPlatform().getSrcAddress()) == 2 && unsigned(msg->getHeader().getDestAddr().getShortAddress()) == 1){
            msg->setPayloadData(symbolToSend(), ((msg->getTotalSymbols() - 120U) / 2));
        }
        return msg;
    }

    char CovertHelper::symbolToSend(){
        messageToEncode = "abcabc";
        messageToEncodeInPayload = "confidencial";

        char tmp = ',';

        if(currentSymbol >= 0){
            tmp = encodeMessage(messageToEncodeInPayload, messageToEncode)[currentSymbol];
        }

        std::cout << "PPPPPPPPPPPPPP  " << encodeMessage(messageToEncodeInPayload, messageToEncode)  << "  " << unsigned(messageToEncode[currentSymbol]) << "  " << unsigned(messageToEncodeInPayload[currentSymbol]) << "  " << unsigned(tmp) << std::endl;

        currentSymbol++;
        if(currentSymbol >= 12)
            currentSymbol = -1;

        return tmp;
    }

    std::string CovertHelper::encodeMessage(std::string msg, std::string key){
        std::string finalMsg;
        for(int i=0; i < msg.length(); i += key.length()){
            std::string tmp = msg.substr(i, i + key.length());
            for(int j=0; j < key.length(); j++)
                finalMsg += (tmp[j] ^ key[j]);
        }
        return finalMsg;
    }

    void CovertHelper::insertDelayPayload(IDSMEMessage* msg) {
        LOG_DEBUG("LALALALALALALALALAALLALALALALALALALALALALALALALALALALALALALALALALALALALA");
        std::ofstream myfile;
        //lastMessage = msg;
        //lastResponse = response;
        messageToEncode = "abcabc,";
        messageToEncodeInPayload = "confidencial,";

        diffChars.clear();
        diffChars.push_back('a');
        diffChars.push_back('b');
        diffChars.push_back('c');
        diffChars.push_back(',');

        diffIntervals.clear();
        diffIntervals.push_back(2);
        diffIntervals.push_back(3);
        diffIntervals.push_back(4);
        diffIntervals.push_back(5);

        std::ifstream file("tmp.csv");

        // 1 symbol --> 4 bits
        // 2 symbols --> 1 byte 

        ///////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////TESTE//////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////

        if(unsigned(this->dsme.getPlatform().getSrcAddress()) == 2 && unsigned(msg->getHeader().getDestAddr().getShortAddress()) == 1){
        
                while (file >> allPackets){
                    allPackets+= ((msg->getTotalSymbols() - 120U) / 2);
                }

                myfile.open ("tmp.csv");
                myfile << std::to_string(allPackets);
                myfile.close(); 

                LOG_DEBUG("ADDRESS: " << unsigned(this->dsme.getPlatform().getSrcAddress()));
                //LOG_DEBUG("MESSAGE TO SEND: " << messageInBits);
                LOG_DEBUG("LETTER TO SEND: " << messageToEncode[letterCounter]);
                LOG_DEBUG("COUNTER: " << bitCounter);
                //LOG_DEBUG("BIT TO SENDDDDD: " << messageInBits.to_string()[bitCounter]);
                LOG_DEBUG("SIZEEEEEE: " << msg->getTotalSymbols());

                uint8_t so = unsigned(this->dsme.getMAC_PIB().macSuperframeOrder);
                // base superframe duration = 960
                // base slot duration = 60
                // 1.5% da slot --> delay singular
                LOG_DEBUG("LIFSSSSSSS: " << unsigned(const_redefines::macLIFSPeriod));
                uint32_t symbols = const_redefines::macLIFSPeriod;//60U * (pow(2, unsigned(so))) * 0.015;

                double millis = (double) omnetpp::simTime().dbl();

                LOG_DEBUG("INTERVALLLLLLLLLLLLLL: " << (millis - lastTime)*1000);
                if(!first && ((millis - lastTime)*1000 > 100)){
                    /*if(bitCounter == 0){
                        letterCounter--;
                        //messageInBits = getBitsetFromChar(messageToEncode[letterCounter]);
                        bitCounter = 7;
                        bitssent--;
                    }else{
                        bitCounter--;
                    }*/
                    if(letterCounter == 0)
                        letterCounter = 7;
                    letterCounter--;
                    covertPacketsCounter--;
                    bitssent--;
                }
                auto lalala = std::find(diffChars.begin(), diffChars.end(), messageToEncode[letterCounter]);
                int x = lalala - diffChars.begin();
                std::cout << "22222222222222222" << std::endl;
                if(first){
                    //ifsDuration = (uint16_t) (700U);
                    lastTime = millis;
                    first = false;
                    covertPackets = 0;
                    covertPacketsCounter = 0;
                    this->dsme.getPlatform().startDelay(NOW + unsigned(symbols));
                    return;
                }else{
                    LOG_DEBUG("ENTROU");
                    symbols = const_redefines::macLIFSPeriod * unsigned(diffIntervals[x]);//60U * (pow(2, unsigned(so))) * 0.015;
                    LOG_DEBUG("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
                    LOG_DEBUG("INDEX = " << x);
                    LOG_DEBUG("INTERVAL = " << unsigned(diffIntervals[x]));
                    std::cout << "COISE-E-TAL   " << letterCounter << "   " << messageToEncode[letterCounter] << "   " << x << "   " << unsigned(diffIntervals[x]) << std::endl;
                    letterCounter++;
                    if(letterCounter >= 7)
                        letterCounter = 0;
                    lastTime = millis;
                    covertPackets++;
                    covertPacketsCounter++;
                    bitssent++;
                    myfile.open ("results/writer" + std::to_string(so) + ".csv", std::ios_base::app);
                    myfile << (std::to_string(millis) + "," + std::to_string(covertPacketsCounter) +  "," + std::to_string(allPackets) + "," + std::to_string(bitssent) + ",");
                    myfile.close(); 
                    //ifsDuration = (uint16_t) (300U);
                    //this->dsme.getEventDispatcher().setupIFSTimer(false);
                    //std::this_thread::sleep_for(std::chrono::milliseconds(100));
                    this->dsme.getPlatform().startDelay(NOW + unsigned(symbols));
                    return;
                }
        }
        this->dsme.getMessageDispatcher().sendPreparedMessage2();
    }



    void CovertHelper::autentication(IDSMEMessage* msg) { // 2 bits
        uint32_t symbols = 1;
        double millis = (double) omnetpp::simTime().dbl();
        
        
        uint32_t diffIntervals[Bits_1][Times];
        std::vector<std::string> possibleCombinations {
            "0",
            "1",
            

            
            
        };


        if(unsigned(this->dsme.getPlatform().getSrcAddress()) == 2 && unsigned(msg->getHeader().getDestAddr().getShortAddress()) == 1){

            int counter = 2;

            for(int i=0; i<Times; i++){
                for(int j=0; j<Bits_1; j++){                 //preencher a matriz cm os tempos
                    diffIntervals[j][i] = counter;
                    counter+=ALPHA;
                }
            }

            

        if(first){
                    //ifsDuration = (uint16_t) (700U);
                    std::cout << "FIRST - " << unsigned(symbols) << std::endl;   //para o primeiro pacote
                    lastTime = millis;
                    first = false;
                    covertPackets = 0;
                    covertPacketsCounter = 0;
                    this->dsme.getPlatform().startDelay(NOW + unsigned(symbols));
                    return;
                }else {    
        int x;
        std::random_device dev;
        std::mt19937 rng(dev());
        std::uniform_int_distribution<std::mt19937::result_type> dist6(0,Times-1);
        auto lala = dist6(rng);

        std::string Code_message1 = diffCodes[unsigned(this->dsme.getPlatform().getSrcAddress())-1][0];
        std::string Code_message2 = diffCodes[unsigned(msg->getHeader().getDestAddr().getShortAddress())-1][0];
        std::string Code_message = Code_message1 + Code_message2;
        double millis = (double) omnetpp::simTime().dbl();

        if(!first && ((millis - lastTime)*1000 > 100)){
            if(letterCounter == 0)
                letterCounter = numero;
            letterCounter-=1;
            
        }

            for(int i=0; i<possibleCombinations.size(); i++)
                if((std::string(1,Code_message.at(letterCounter))).compare(possibleCombinations.at(i)) == 0)
                    x = diffIntervals[i][lala];

        letterCounter+=1;
            if(letterCounter >= numero){
                    letterCounter = 0;
                    //Autentication = true;
        }
                
        lastTime = millis;


       



        this->dsme.getPlatform().startDelay(NOW + unsigned(x));
        return;
                }
    }
    this->dsme.getMessageDispatcher().sendDoneGTS();
    }



     void CovertHelper::autentication2(IDSMEMessage* msg) { // 2 bits
        uint32_t symbols = 1;
        double millis = (double) omnetpp::simTime().dbl();
        
        
        uint32_t diffIntervals[Bits_2][Times];
        std::vector<std::string> possibleCombinations {
            "00",
            "01",
            "10",
            "11",

            
            
        };

        std::cout << unsigned(this->dsme.getPlatform().getSrcAddress()) << std::endl;
        std::cout << unsigned(msg->getHeader().getDestAddr().getShortAddress())<< std::endl;
        if(unsigned(this->dsme.getPlatform().getSrcAddress()) == 2 && unsigned(msg->getHeader().getDestAddr().getShortAddress()) == 1){

            int counter = 2;

            for(int i=0; i<Times; i++){
                for(int j=0; j<Bits_2; j++){                 //preencher a matriz cm os tempos
                    diffIntervals[j][i] = counter;
                    counter+=ALPHA;
                }
            }


            

        if(first){
                    //ifsDuration = (uint16_t) (700U);
                    std::cout << "FIRST - " << unsigned(symbols) << std::endl;   //para o primeiro pacote
                    lastTime = millis;
                    first = false;    
                    covertPackets = 0;
                    covertPacketsCounter = 0;
                    this->dsme.getPlatform().startDelay(NOW + unsigned(symbols));
                    return;
                }else {    
        int x;
        std::random_device dev;
        std::mt19937 rng(dev());
        std::uniform_int_distribution<std::mt19937::result_type> dist6(0,Times-1);
        auto lala = dist6(rng);

        std::string Code_message1 = diffCodes[unsigned(this->dsme.getPlatform().getSrcAddress())-1][0];
        std::string Code_message2 = diffCodes[unsigned(msg->getHeader().getDestAddr().getShortAddress())-1][0];
        std::string Code_message = Code_message1 + Code_message2;

        double millis = (double) omnetpp::simTime().dbl();

        if(!first && ((millis - lastTime)*1000 > 100)){
            if(letterCounter == 0)
                letterCounter = numero;
            letterCounter-=2;
        }

            for(int i=0; i<possibleCombinations.size(); i++)
                if((std::string(1,Code_message.at(letterCounter))
                                + Code_message.at(letterCounter+1)).compare(possibleCombinations.at(i)) == 0)
                    x = diffIntervals[i][lala];

        letterCounter+=2;
            if(letterCounter >= numero){
                    letterCounter = 0;
                    //Autentication = true;
        }
                
        lastTime = millis;

       

std::cout << x << std::endl;

        this->dsme.getPlatform().startDelay(NOW + x);
        return;
                }
    }
    this->dsme.getMessageDispatcher().sendDoneGTS();
    }

    
      void CovertHelper::autentication4(IDSMEMessage* msg) { // 4 bits
        uint32_t symbols = 1;
        double millis = (double) omnetpp::simTime().dbl();
        
        
        
        uint32_t diffIntervals[Bits][Times];
        std::vector<std::string> possibleCombinations {
            "0000",
            "0001",
            "0010",
            "0011",
            "0100",
            "0101",
            "0110",
            "0111",
            "1000",
            "1001",
            "1010",
            "1011",
            "1100",
            "1101",
            "1110",
            "1111",
            
        };


        if(unsigned(this->dsme.getPlatform().getSrcAddress()) == 2 && unsigned(msg->getHeader().getDestAddr().getShortAddress()) == 1){

            int counter = 2;

            for(int i=0; i<Times; i++){
                for(int j=0; j<Bits; j++){                 //preencher a matriz cm os tempos
                    diffIntervals[j][i] = counter;
                    counter+=ALPHA;
                }
            }

            

        if(first){
                    //ifsDuration = (uint16_t) (700U);
                    std::cout << "FIRST - " << unsigned(symbols) << std::endl;   //para o primeiro pacote
                    lastTime = millis;
                    first = false;
                    covertPackets = 0;
                    covertPacketsCounter = 0;
                    this->dsme.getPlatform().startDelay(NOW + unsigned(symbols));
                    return;
                }else {    
        int x;
        std::random_device dev;
        std::mt19937 rng(dev());
        std::uniform_int_distribution<std::mt19937::result_type> dist6(0,Times-1);
        auto lala = dist6(rng);

        std::string Code_message1 = diffCodes[unsigned(this->dsme.getPlatform().getSrcAddress())-1][0];
        std::string Code_message2 = diffCodes[unsigned(msg->getHeader().getDestAddr().getShortAddress())-1][0];
        std::string Code_message = Code_message1 + Code_message2;

        if(!first && ((millis - lastTime)*1000 > 100)){
            if(letterCounter == 0)
                letterCounter = numero;
            letterCounter-=4;
            
        }

        std::cout << "LC: " << letterCounter << std::endl;
        std::cout << "CM: " << Code_message << std::endl;
        std::cout << "BITS: " << Code_message.at(letterCounter) << Code_message.at(letterCounter+1) << Code_message.at(letterCounter+2) << Code_message.at(letterCounter+3) << std::endl;

            for(int i=0; i<possibleCombinations.size(); i++)
                if((std::string(1,Code_message.at(letterCounter))
                                + Code_message.at(letterCounter+1)
                                + Code_message.at(letterCounter+2)
                                + Code_message.at(letterCounter+3)).compare(possibleCombinations.at(i)) == 0)
                    x = diffIntervals[i][lala];

        letterCounter+=4;
            if(letterCounter >= numero){
                    letterCounter = 0;
                    //Autentication = true;
        }
                
        lastTime = millis;

       



        this->dsme.getPlatform().startDelay(NOW + unsigned(x));
        return;
                }

    }
    this->dsme.getMessageDispatcher().sendDoneGTS();
    }
    



    void CovertHelper::autentication8(IDSMEMessage* msg) { // 8 bits
        uint32_t symbols = 1;
        double millis = (double) omnetpp::simTime().dbl();
        
        
        uint32_t diffIntervals[BYTE_NUM_8][Times];
        
        


        if(unsigned(this->dsme.getPlatform().getSrcAddress()) == 2 && unsigned(msg->getHeader().getDestAddr().getShortAddress()) == 1){

            int counter = 2;

            for(int i=0; i<Times; i++){
                for(int j=0; j<BYTE_NUM_8; j++){                 //preencher a matriz cm os tempos
                    diffIntervals[j][i] = counter;
                    counter+=ALPHA;
                }
            }

            

                
        

        

        if(first){
                    //ifsDuration = (uint16_t) (700U);
                    std::cout << "FIRST - " << unsigned(symbols) << std::endl;   //para o primeiro pacote
                    lastTime = millis;
                    first = false;
                    covertPackets = 0;
                    covertPacketsCounter = 0;
                    this->dsme.getPlatform().startDelay(NOW + unsigned(symbols));
                    return;
                }else {    
        int x;
        std::random_device dev;
        std::mt19937 rng(dev());
        std::uniform_int_distribution<std::mt19937::result_type> dist6(0,Times-1);
        auto lala = dist6(rng);

        std::string Code_message1 = diffCodes[unsigned(this->dsme.getPlatform().getSrcAddress())-1][0];
        std::string Code_message2 = diffCodes[unsigned(msg->getHeader().getDestAddr().getShortAddress())-1][0];
        std::string Code_message = Code_message1 + Code_message2;

        if(!first && ((millis - lastTime)*1000 > 100)){
            if(letterCounter == 0)
                letterCounter = numero;
            letterCounter-=8;
            
        }

            for(int i=0; i<possibleCombinations8.size(); i++)
                if((std::string(1,Code_message.at(letterCounter))
                                + Code_message.at(letterCounter+1)
                                + Code_message.at(letterCounter+2)
                                + Code_message.at(letterCounter+3)
                                + Code_message.at(letterCounter+4)
                                + Code_message.at(letterCounter+5)
                                + Code_message.at(letterCounter+6)
                                + Code_message.at(letterCounter+7)).compare(possibleCombinations8.at(i)) == 0)
                    x = diffIntervals[i][lala];

        letterCounter+=8;
            if(letterCounter >= numero){
                    letterCounter = 0;
                    //Autentication = true;
        }
                
        lastTime = millis;

       



        this->dsme.getPlatform().startDelay(NOW + unsigned(x));
        return;
                }

                

    }
    this->dsme.getMessageDispatcher().sendDoneGTS();
    }
    

}












